-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 08, 2017 at 04:48 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `im_messenger`
--
CREATE DATABASE IF NOT EXISTS `im_messenger` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `im_messenger`;

-- --------------------------------------------------------

--
-- Table structure for table `admingroup`
--

CREATE TABLE `admingroup` (
  `id` int(11) UNSIGNED NOT NULL,
  `adminType` int(11) NOT NULL,
  `adminInfo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admingroup`
--

INSERT INTO `admingroup` (`id`, `adminType`, `adminInfo`) VALUES
(1, 0, 'superUser'),
(2, 1, 'subUser'),
(3, 2, 'supersuperUser');

-- --------------------------------------------------------

--
-- Table structure for table `admintype`
--

CREATE TABLE `admintype` (
  `id` int(11) UNSIGNED NOT NULL,
  `adminId` int(11) NOT NULL,
  `adminType` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admintype`
--

INSERT INTO `admintype` (`id`, `adminId`, `adminType`) VALUES
(4, 7, 2),
(5, 8, 0),
(6, 9, 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_contactinfo`
--

CREATE TABLE `admin_contactinfo` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(500) NOT NULL,
  `phone` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin_contactinfo`
--



-- --------------------------------------------------------

--
-- Table structure for table `friend_list`
--

CREATE TABLE `friend_list` (
  `serial` int(11) UNSIGNED NOT NULL,
  `userId` int(11) UNSIGNED NOT NULL,
  `friendId` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `friend_list`
--



-- --------------------------------------------------------

--
-- Table structure for table `im_group`
--

CREATE TABLE `im_group` (
  `g_id` int(11) UNSIGNED NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `createdBy` int(11) UNSIGNED NOT NULL,
  `lastActive` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `im_group`
--



-- --------------------------------------------------------

--
-- Table structure for table `im_group_members`
--

CREATE TABLE `im_group_members` (
  `serial` int(11) UNSIGNED NOT NULL,
  `g_id` int(11) UNSIGNED NOT NULL,
  `u_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `im_group_members`
--



-- --------------------------------------------------------

--
-- Table structure for table `im_message`
--

CREATE TABLE `im_message` (
  `m_id` int(11) UNSIGNED NOT NULL,
  `sender` int(11) UNSIGNED NOT NULL,
  `receiver` int(11) UNSIGNED NOT NULL,
  `message` varchar(500) NOT NULL,
  `type` varchar(20) NOT NULL,
  `link` varchar(500) DEFAULT NULL,
  `linkData` text,
  `receiver_type` enum('group','user') NOT NULL DEFAULT 'user',
  `date` date NOT NULL,
  `time` time NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP 
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `im_message`
--

-- --------------------------------------------------------

--
-- Table structure for table `im_receiver`
--

CREATE TABLE `im_receiver` (
  `serial` int(11) UNSIGNED NOT NULL,
  `g_id` int(11) UNSIGNED NOT NULL,
  `m_id` int(11) UNSIGNED NOT NULL,
  `r_id` int(11) UNSIGNED NOT NULL,
  `received` int(1) NOT NULL,
  `time` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `im_receiver`
--


-- --------------------------------------------------------

--
-- Table structure for table `im_usersocket`
--

CREATE TABLE `im_usersocket` (
  `serial` int(11) UNSIGNED NOT NULL,
  `userId` int(11) UNSIGNED NOT NULL,
  `socketId` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `im_usersocket`
--

INSERT INTO `im_usersocket` (`serial`, `userId`, `socketId`) VALUES
(51, 4, 'W0sHGFvQWZWxoTHVAAAI');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(11);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userId` int(11) UNSIGNED NOT NULL,
  `userSecret` varchar(255) NOT NULL,
  `firstName` varchar(100) NOT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `userEmail` varchar(100) NOT NULL,
  `userPassword` varchar(255) NOT NULL,
  `userMobile` varchar(30) DEFAULT NULL,
  `userDateOfBirth` timestamp NULL DEFAULT NULL,
  `userGender` varchar(15) DEFAULT NULL,
  `userStatus` int(11) DEFAULT NULL,
  `userVerification` int(11) DEFAULT NULL,
  `userAddress` varchar(255) DEFAULT NULL,
  `userProfilePicture` varchar(255) DEFAULT NULL,
  `userResetToken` varchar(255) DEFAULT NULL,
  `userType` int(11) NOT NULL,
  `userRole` int(2) NOT NULL,
  `lastModified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `userSecret`, `firstName`, `lastName`, `userEmail`, `userPassword`, `userMobile`, `userDateOfBirth`, `userGender`, `userStatus`, `userVerification`, `userAddress`, `userProfilePicture`, `userResetToken`, `userType`, `userRole`, `lastModified`) VALUES
(4, 'uEzOZUrL3h', 'simon', 'hasan', 'simon@gmail.com', '$2y$10$.qRXVQTWkTe2oJTOb/UJbeI9DqcfXyTE4VURlFwrOWuk0u68otW3S', NULL, '0000-00-00 00:00:00', NULL, 1, 1, NULL, '06152017165714profile.jpg', NULL, 1, 1, '2017-06-15 10:55:57'),
(5, 'fX5mHGD7UW', 'anisul', 'hoque', 'anis@gmail.com', '$2y$10$.qRXVQTWkTe2oJTOb/UJbeI9DqcfXyTE4VURlFwrOWuk0u68otW3S', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, 1, 1, '2017-04-05 14:47:40'),
(7, 'MdE9UegmMV', 'admin', '', 'admin@admin.com', '$2y$10$.qRXVQTWkTe2oJTOb/UJbeI9DqcfXyTE4VURlFwrOWuk0u68otW3S', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, 0, 1, '2017-04-05 14:48:20'),
(8, 'kYWMOKog8h', 'anis', NULL, 'anis@admin.com', '$2y$10$.qRXVQTWkTe2oJTOb/UJbeI9DqcfXyTE4VURlFwrOWuk0u68otW3S', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, 0, 0, '2017-06-19 13:54:05'),
(9, 'P7mhaYX5L0', 'test', NULL, 'simon@admin.com', '$2y$10$.qRXVQTWkTe2oJTOb/UJbeI9DqcfXyTE4VURlFwrOWuk0u68otW3S', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, 0, 0, '2017-06-19 13:55:26');

-- --------------------------------------------------------

--
-- Table structure for table `users_roles`
--

CREATE TABLE `users_roles` (
  `type` int(2) UNSIGNED NOT NULL,
  `role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_roles`
--

INSERT INTO `users_roles` (`type`, `role`) VALUES
(0, 'ROLE_ADMIN'),
(1, 'ROLE_USER');

-- --------------------------------------------------------

--
-- Table structure for table `user_device`
--

CREATE TABLE `user_device` (
  `serial` int(11) UNSIGNED NOT NULL,
  `userId` int(11) UNSIGNED NOT NULL,
  `deviceId` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admingroup`
--
ALTER TABLE `admingroup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admintype`
--
ALTER TABLE `admintype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_contactinfo`
--
ALTER TABLE `admin_contactinfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `friend_list`
--
ALTER TABLE `friend_list`
  ADD PRIMARY KEY (`serial`);

--
-- Indexes for table `im_group`
--
ALTER TABLE `im_group`
  ADD PRIMARY KEY (`g_id`),
  ADD KEY `createdBy` (`createdBy`);

--
-- Indexes for table `im_group_members`
--
ALTER TABLE `im_group_members`
  ADD PRIMARY KEY (`serial`);

--
-- Indexes for table `im_message`
--
ALTER TABLE `im_message`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `im_receiver`
--
ALTER TABLE `im_receiver`
  ADD PRIMARY KEY (`serial`);

--
-- Indexes for table `im_usersocket`
--
ALTER TABLE `im_usersocket`
  ADD PRIMARY KEY (`serial`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `users_roles`
--
ALTER TABLE `users_roles`
  ADD PRIMARY KEY (`type`);

--
-- Indexes for table `user_device`
--
ALTER TABLE `user_device`
  ADD PRIMARY KEY (`serial`),
  ADD KEY `userId` (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admingroup`
--
ALTER TABLE `admingroup`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `admintype`
--
ALTER TABLE `admintype`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `admin_contactinfo`
--
ALTER TABLE `admin_contactinfo`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `friend_list`
--
ALTER TABLE `friend_list`
  MODIFY `serial` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `im_group`
--
ALTER TABLE `im_group`
  MODIFY `g_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `im_group_members`
--
ALTER TABLE `im_group_members`
  MODIFY `serial` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `im_message`
--
ALTER TABLE `im_message`
  MODIFY `m_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `im_receiver`
--
ALTER TABLE `im_receiver`
  MODIFY `serial` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `im_usersocket`
--
ALTER TABLE `im_usersocket`
  MODIFY `serial` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `user_device`
--
ALTER TABLE `user_device`
  MODIFY `serial` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `im_group`
--
ALTER TABLE `im_group`
  ADD CONSTRAINT `im_group_ibfk_1` FOREIGN KEY (`createdBy`) REFERENCES `users` (`userId`);

--
-- Constraints for table `user_device`
--
ALTER TABLE `user_device`
  ADD CONSTRAINT `user_device_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
