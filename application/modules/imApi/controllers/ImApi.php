<?php
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version2X;

class ImApi extends REST_Controller{

    //for requesting model class and checking user authentication
    public function __construct()
    {
        parent::__construct();

        $this->load->model("Im_group_Model");
        $this->load->model("Im_group_members_Model");
        $this->load->model("Im_message_Model");
        $this->load->model("User_Model");
        $this->load->model("Im_receiver_Model");

        $headers = apache_request_headers();
        if (isset($headers["Authorizationkeyfortoken"])) {
            if (!$this->User_Model->isValidToken($headers["Authorizationkeyfortoken"])) {
                $response = array(
                    "stauts" => array(
                        "code" => REST_Controller::HTTP_UNAUTHORIZED,
                        "message" => "Unauthorized"
                    ),
                    "response" => null
                );
                $this->response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }
        } else {
            $response = array(
                "stauts" => array(
                    "code" => REST_Controller::HTTP_UNAUTHORIZED,
                    "message" => "Unauthorized"
                ),
                "response" => null
            );
            $this->response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
    }
    public function getMembers_get(){
        $headers = apache_request_headers();
        $userId = $this->User_Model->getTokenToId($headers["Authorizationkeyfortoken"]);
        $g_id=$this->get("groupId");
        if($g_id==null){
            $response = array(
                "status" => array(
                    "code" => REST_Controller::HTTP_NOT_ACCEPTABLE,
                    "message" => "Error "
                ),
                "response" => "groupId is required"
            );
            $this->response($response, REST_Controller::HTTP_NOT_ACCEPTABLE);
            return ;
        }
        $membersInfo=array();

        $members=$this->Im_group_members_Model->getMembersWihoutSender($g_id,$userId);

        foreach ($members as $u_id){
            $membersInfo[]=$this->User_Model->get_user($u_id->u_id,null,null);
        }

        $meCreator=$this->Im_group_Model->ifThisUserCreator($g_id,$userId);
        $response = array(
            "status" => array(
                "code" => REST_Controller::HTTP_OK,
                "message" => "Success"
            ),
            "response" =>array(
                "meCreator"=>$meCreator,
                "memberList"=>$membersInfo
            )

        );
        $this->response($response, REST_Controller::HTTP_OK);
    }
    public function deleteMember_post(){  // delete a member from the group
        $headers = apache_request_headers();
        $userId = $this->User_Model->getTokenToId($headers["Authorizationkeyfortoken"]);
        $this->form_validation->set_rules('memberId', 'memberId', 'required');
        $this->form_validation->set_rules('groupId', 'groupId', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response = array(
                "status" => array(
                    "code" => REST_Controller::HTTP_NOT_ACCEPTABLE,
                    "message" => "Validation Error"
                ),
                "response" => validation_errors()
            );
            $this->response($response, REST_Controller::HTTP_NOT_ACCEPTABLE);
            return;
        }
        $memberId=$this->post("memberId");
        $groupId=$this->post("groupId");

        //$memberList=$this->Im_group_members_Model->getMembers($groupId);
        $meCreator=$this->Im_group_Model->ifThisUserCreator($groupId,$userId);
        if(!$meCreator){
            $response = array(
                "status" => array(
                    "code" => REST_Controller::HTTP_UNAUTHORIZED,
                    "message" => "Error "
                ),
                "response" => "Can't delete member"
            );
            $this->response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }

        $memberList=$this->Im_group_members_Model->getMembers($groupId);
        $this->Im_group_members_Model->delete($groupId,$memberId);
        $this->Im_receiver_Model->DeleteAll($groupId,$memberId);
        $membersInfo=array();

        $members=$this->Im_group_members_Model->getMembersWihoutSender($groupId,$userId);
        foreach ($members as $u_id){
            $membersInfo[]=$this->User_Model->get_user($u_id->u_id,null,null);
        }

        $creatorId=$this->Im_group_Model->get($groupId)->createdBy;
        $meCreator=false;
        if($userId==$creatorId){
            $meCreator=true;
        }
        $registerData=array(
            "_r"=>$headers["Authorizationkeyfortoken"]
        );

        $updateData=array(
            "_r"=>$headers["Authorizationkeyfortoken"],
            "groupId"=>$groupId,
            "memberIds"=>$memberList
        );
        $client = new Client(new Version2X($this->config->item('socket_url')));
        $client->initialize();
        $client->emit("register",$registerData);
        $client->emit("updateMember",$updateData);
        $client->close();
        $response=array(
            "status" => array(
                "code" => REST_Controller::HTTP_OK,
                "message" => "Success"
            ),
            "response"=>array(
                "meCreator"=>$meCreator,
                "memberList"=>$membersInfo
            )

        );
        $this->response($response, REST_Controller::HTTP_OK);
    }

    public function getGroups_get(){  //get all groups
        $headers = apache_request_headers();
        $userId = $this->User_Model->getTokenToId($headers["Authorizationkeyfortoken"]);
        $limit=$this->get("limit");
        $start=$this->get("start");
        $group_ids=$this->Im_group_members_Model->getGroups($userId,$limit,$start);
        $groups=array();
        foreach ($group_ids as $g_id){
            $membersInfo=array();
            $groupImage=array();
            $groupInfo=$this->Im_group_Model->get($g_id->g_id);
            $lastActive=$groupInfo->lastActive;
            $groupName=$groupInfo->name;


            $recentMessage=$this->Im_message_Model->getRecentMessage($g_id->g_id);

            $members=$this->Im_group_members_Model->getMembersWihoutSender($g_id->g_id,$userId);
            foreach ($members as $u_id){
                $membersInfo[]=$this->User_Model->get_user($u_id->u_id,null,null);
            }
            $totalMember=$this->Im_group_members_Model->getTotalGroupMember($g_id->g_id);
            if($totalMember>1) {
                if ($totalMember >= 4) {
                    for ($i = 0; $i < 3; $i++) {
                        $groupImage[] = $membersInfo[$i]['profilePictureUrl'];
                    }
                } else if ($totalMember >= 3) {
                    for ($i = 0; $i < 2; $i++) {
                        $groupImage[] = $membersInfo[$i]['profilePictureUrl'];
                    }
                } else if ($totalMember <= 2) {
                    $groupImage[] = $membersInfo[0]['profilePictureUrl'];
                }
                $totalMember = $totalMember - 1;
            }
            else {
                $groupImage[]=base_url()."assets/img/download.png";
                if($groupName==null||$groupName==""|| $groupName=='""'|| $groupName=="''") {
                    $groupName = "No Member";
                }
            }

            if($groupName==null ||$groupName==""|| $groupName=='""'|| $groupName=="''"){
                $groupName="";
                if($totalMember<=2){
                    for ($i=0;$i<$totalMember;$i++){
                        if($i==($totalMember-1)){
                            $groupName.=" ".$membersInfo[$i]['firstName'];
                        }
                        else{
                            $groupName.=" ".$membersInfo[$i]['firstName'].",";
                        }
                    }
                }elseif ($totalMember>=3){
                    for ($i=0;$i<$totalMember;$i++){
                        if($i==($totalMember-1)){
                            $groupName.=" ".$membersInfo[$i]['firstName'];
                        }
                        else{
                            $groupName.=" ".$membersInfo[$i]['firstName'].",";
                        }
                    }
                }else{
                    $groupName = "No Member";
                }
            }
            $lastActive=date_format(date_create($lastActive),DATE_ISO8601);
            if($recentMessage==null){
                $groups[]=array(
                    "groupId"=>(int)$g_id->g_id,
                    "groupImage"=>$groupImage,
                    "groupName"=>trim($groupName),
                    //"totalMember"=>$totalMember,
                    "lastActive"=>$lastActive,
                    //"members"=>$membersInfo,
                    //"me"=>$me,
                    "recentMessage"=>null,
                    "messageType"=>null,
                    //"messageDateTime"=>$recentMessage->date_time,
                );
            }else{
                $groups[]=array(
                    "groupId"=>(int)$g_id->g_id,
                    "groupImage"=>$groupImage,
                    "groupName"=>trim($groupName),
                    //"totalMember"=>$totalMember,
                    "lastActive"=>$lastActive,
                    //"members"=>$membersInfo,
                    //"me"=>$me,
                    "recentMessage"=>$recentMessage->message,
                    "messageType"=>$recentMessage->type,
                    //"messageDateTime"=>$recentMessage->date_time,
                );
            }

        }
        $response = array(
            "status" => array(
                "code" => REST_Controller::HTTP_OK,
                "message" => "Success"
            ),
            "response" =>$groups
        );
        $this->response($response, REST_Controller::HTTP_OK);
    }

    public function changeGroupName_post(){
        $this->form_validation->set_rules('groupName', 'groupName', 'required');
        $this->form_validation->set_rules('groupId', 'groupId', 'required');
        $headers = apache_request_headers();
        if ($this->form_validation->run() == FALSE) {
            $response = array(
                "status" => array(
                    "code" => REST_Controller::HTTP_NOT_ACCEPTABLE,
                    "message" => "Validation Error"
                ),
                "response" => validation_errors()
            );
            $this->response($response, REST_Controller::HTTP_NOT_ACCEPTABLE);
            return;
        }
        $groupName=$this->post("groupName");
        if($groupName==""|| $groupName=='""'|| $groupName=="''"){
            $groupName=null;
        }
        $groupId=$this->post("groupId");
        $this->Im_group_Model->update($groupId,$groupName);
        $memberList=$this->Im_group_members_Model->getMembers($groupId);
        $updateData=array(
            "_r"=>$headers["Authorizationkeyfortoken"],
            "groupId"=>$groupId,
            "memberIds"=>$memberList
        );
        $registerData=array(
            "_r"=>$headers["Authorizationkeyfortoken"]
        );
        $client = new Client(new Version2X($this->config->item('socket_url')));
        $client->initialize();
        $client->emit("register",$registerData);
        $client->emit("updateMember",$updateData);
        $client->close();
        $response = array(
            "status" => array(
                "code" => REST_Controller::HTTP_OK,
                "message" => "Success"
            ),
            "response" =>null

        );
        $this->response($response, REST_Controller::HTTP_OK);
    }

    //get pending not pending messages
    public function getMessage_get(){ //groupId or usersId[] one must not null, date, time
        $headers = apache_request_headers();
        $r_id = $this->User_Model->getTokenToId($headers["Authorizationkeyfortoken"]);

        $g_id=$this->get("groupId");
        $start=$this->get("start");
        $limit=$this->get("limit");
        
        if($g_id==null){
            $users=$this->get("users");
            if($users==null){
                $response = array(
                    "status" => array(
                        "code" => REST_Controller::HTTP_NOT_ACCEPTABLE,
                        "message" => "Error "
                    ),
                    "response" => "Either user ids or receiver id is required"
                );
                $this->response($response, REST_Controller::HTTP_NOT_ACCEPTABLE);
                return;
            }
            $groupsIds=$this->Im_group_members_Model->getGroups($users,null,null);
            foreach ($groupsIds as $groupId){
                $totalReceiver=$this->Im_group_members_Model->getTotalGroupMember($groupId->g_id);
                if(($totalReceiver-1)==count($users)){
                    $g_id=$groupId->g_id;
                    break;
                }
            }
        }
        $messages=$this->Im_message_Model->getMessage($g_id,$start,$limit);  //get messages
        $totalMessage=$this->Im_message_Model->getTotalMessage($g_id);
        $data=[];
        foreach ($messages as $message){
            $messageExist=$this->Im_receiver_Model->isExsist($message->m_id,$r_id,$g_id);
            $messageNotReceived=$this->Im_receiver_Model->isNotReceived($r_id,$g_id,$message->m_id);
           if($messageExist && $messageNotReceived){
               $this->Im_receiver_Model->update($r_id,$g_id,$message->m_id);
           }
            $senderProfile=$this->User_Model->get_user($message->sender,null,null);
            $ios_date_time =date_format(date_create($message->date_time),DATE_ISO8601);
            $message->ios_date_time=$ios_date_time;

            $data[]=array(
                "sender"=>$senderProfile,
                "message"=>$message
            );


        }

        $response = array(
            "status" => array(
                "code" => REST_Controller::HTTP_OK,
                "message" => "Success"
            ),
            "totalMessage"=>$totalMessage,
            "response" =>array_reverse($data)

        );
        $this->response($response, REST_Controller::HTTP_OK);

    }

    private function getNewGroupInfo($g_id,$userId){
        $membersInfo=array();
        $groupImage=array();
        $groupInfo=$this->Im_group_Model->get($g_id);
        $lastActive=$groupInfo->lastActive;
        $groupName=$groupInfo->name;

        //$me=$this->User_Model->get_user($userId,null,null);
        $recentMessage=$this->Im_message_Model->getRecentMessage($g_id);


        $members=$this->Im_group_members_Model->getMembersWihoutSender($g_id,$userId);
        foreach ($members as $u_id){
            $membersInfo[]=$this->User_Model->get_user($u_id->u_id,null,null);
        }
        $totalMember=$this->Im_group_members_Model->getTotalGroupMember($g_id);
        if($totalMember>1) {
            if ($totalMember >= 4) {
                for ($i = 0; $i < 3; $i++) {
                    $groupImage[] = $membersInfo[$i]['profilePictureUrl'];
                }
            } else if ($totalMember >= 3) {
                for ($i = 0; $i < 2; $i++) {
                    $groupImage[] = $membersInfo[$i]['profilePictureUrl'];
                }
            } else if ($totalMember <= 2) {
                $groupImage[] = $membersInfo[0]['profilePictureUrl'];
            }
            $totalMember = $totalMember - 1;
        }
        else {
            $groupImage[]=base_url()."assets/img/download.png";
            if($groupName==null || $groupName==""|| $groupName=='""'|| $groupName=="''") {
                $groupName = "No Member";
            }
        }

        if($groupName==null|| $groupName==""|| $groupName=='""'|| $groupName=="''"|| $groupName=="''"){
            $groupName="";
            if($totalMember<=2){
                for ($i=0;$i<$totalMember;$i++){
                    if($i==($totalMember-1)){
                        $groupName.=" ".$membersInfo[$i]['firstName'];
                    }
                    else{
                        $groupName.=" ".$membersInfo[$i]['firstName'].",";
                    }
                }
            }elseif ($totalMember>=3){
                for ($i=0;$i<$totalMember;$i++){
                    if($i==($totalMember-1)){
                        $groupName.=" ".$membersInfo[$i]['firstName'];
                    }
                    else{
                        $groupName.=" ".$membersInfo[$i]['firstName'].",";
                    }
                }
            }else{
                $groupName = "No Member";
            }
        }
        $lastActive=date_format(date_create($lastActive),DATE_ISO8601);
        $groups=array(
            "groupId"=>(int)$g_id,
            "groupImage"=>$groupImage,
            "groupName"=>trim($groupName),
            //"totalMember"=>$totalMember,
            "lastActive"=>$lastActive,
            //"members"=>$membersInfo,
            //"me"=>$me,
            "recentMessage"=>$recentMessage->message,
            "messageType"=>$recentMessage->type,
            //"messageDateTime"=>$recentMessage->date_time,
        );
        return $groups;
    }

    public function createGroupByMember_post(){
        $headers = apache_request_headers();
        $senderId = $this->User_Model->getTokenToId($headers["Authorizationkeyfortoken"]);
        $date_time=date(DATE_ISO8601, time());
        $users=$this->post("userId");
        if($users==null){
            $response = array(
                "status" => array(
                    "code" => REST_Controller::HTTP_NOT_ACCEPTABLE,
                    "message" => "Error "
                ),
                "response" => "user ids are required"
            );
            $this->response($response, REST_Controller::HTTP_NOT_ACCEPTABLE);
            return;
        }else{
            $users[]=$senderId;
        }


        $g_ids=array();
        $groupsIds=$this->Im_group_members_Model->getGroups($users,null,null);
        foreach ($groupsIds as $groupId){
            $totalReceiver=$this->Im_group_members_Model->getTotalGroupMember($groupId->g_id);
            $getMembers=$this->Im_group_members_Model->getMembers($groupId->g_id);
            $member=array();
            foreach ($getMembers as $getMember){
                $member[]=$getMember->u_id;
            }
            $diff=array_diff($member,$users);

            if(((int)$totalReceiver)==(count($users)) && count($diff)==0){
                $g_ids[]=$groupId->g_id;
                break;
            }
        }
        if (count($g_ids)>0){
            $receiverId=$g_ids[0];
        }else{
            $name=$this->post("g_name");
            if($name==null || $name==""|| $name=='""'|| $name=="''") {
                $name = null;
            }

            $receiverId=$this->Im_group_Model->insert($name,$date_time,$senderId);
            try{
                foreach ($users as $user){
                    $this->Im_group_members_Model->insert($receiverId,$user);
                }
               // $this->Im_group_members_Model->insert($receiverId,$senderId);
            }catch (Exception $e){
                $this->Im_group_members_Model->DeleteAll($receiverId);
                $this->Im_group_Model->DeleteAll($receiverId);
                $response = array(
                    "status" => array(
                        "code" => REST_Controller::HTTP_NOT_FOUND,
                        "message" => "Success"
                    ),
                    "response" =>"User Not Found"

                );
                $this->response($response, REST_Controller::HTTP_NOT_FOUND);
                return;
            }

        }
        $response = array(
            "status" => array(
                "code" => REST_Controller::HTTP_OK,
                "message" => "Success"
            ),
            "response" =>array(
                "groupId"=>(int)$receiverId
            )

        );
        $this->response($response, REST_Controller::HTTP_OK);
    }

    public function sendMessage_post(){  // groupId(receiverId Id)(null), users[] if  groupId==null , g_name(null), file(Null) or message(null),date(yyyy-mm-dd),time(hh:mm:ss)
        $headers = apache_request_headers();
        $senderId = $this->User_Model->getTokenToId($headers["Authorizationkeyfortoken"]);
        $date=date('Y-m-d');
        $time=date("h:i:s");
        $date_time=date(DATE_ISO8601, time());
        $client = new \Emojione\Client(new \Emojione\Ruleset());
        $client->ascii=true;
       // $client->riskyMatchAscii=true; // if enable http:// also converted to emoji


        $receiverId=$this->post("groupId");
        if($receiverId==null){
            $users=$this->post("userId");
            if($users==null){
                $response = array(
                    "status" => array(
                        "code" => REST_Controller::HTTP_NOT_ACCEPTABLE,
                        "message" => "Error "
                    ),
                    "response" => "Either user ids or receiver id is required"
                );
                $this->response($response, REST_Controller::HTTP_NOT_ACCEPTABLE);
                return;
            }else{
                $users[]=$senderId;
            }


            $g_ids=array();
            $groupsIds=$this->Im_group_members_Model->getGroups($users,null,null);
            foreach ($groupsIds as $groupId){
                $totalReceiver=$this->Im_group_members_Model->getTotalGroupMember($groupId->g_id);
                $getMembers=$this->Im_group_members_Model->getMembers($groupId->g_id);
                $member=array();
                foreach ($getMembers as $getMember){
                    $member[]=$getMember->u_id;
                }
                $diff=array_diff($member,$users);

                if(((int)$totalReceiver)==(count($users)) && count($diff)==0){
                    $g_ids[]=$groupId->g_id;
                    break;
                }
            }
            if (count($g_ids)>0){
                $receiverId=$g_ids[0];
            }else{
                $name=$this->post("g_name");
                if($name==null || $name==""|| $name=='""'|| $name=="''") {
                    $name = null;
                }

                $receiverId=$this->Im_group_Model->insert($name,$date_time,$senderId);
                foreach ($users as $user){
                    $this->Im_group_members_Model->insert($receiverId,$user);
                }
               // $this->Im_group_members_Model->insert($receiverId,$senderId);
            }

        }
        $message=null;
        $image=null;
        $fileType=null;
        $actualFolderName="./assets/im/group_$receiverId";
        if (!is_dir($actualFolderName)) {
            mkdir($actualFolderName, 0777, true);
        }
        $config['upload_path']          = $actualFolderName;
        $config['allowed_types']        = '*';
        $config['file_name']            =  date("mjYGis")."im".$this->User_Model->generateRandomString(5).$receiverId ;
        $config['max_size']             = '';


        $this->load->library('upload', $config);
        if(isset($_FILES['file']['tmp_name'])&& !empty($_FILES['file']['tmp_name'])) {
            if (!$this->upload->do_upload('file')) {
                $response = array(
                    "status" => array(
                        "code" => REST_Controller::HTTP_BAD_REQUEST,
                        "message" => "File upload Error"
                    ),
                    "response" => $this->upload->display_errors()
                );
                $this->response($response, REST_Controller::HTTP_BAD_REQUEST);

            } else {
                //here $file_data receives an array that has all the info
                //pertaining to the upload, including 'file_name'
                $file_data = $this->upload->data();
                if($file_data["file_type"] == "audio/mp3" || $file_data["file_type"] == "audio/mpeg3" || $file_data["file_type"] == "audio/mpg" || $file_data["file_type"] == "audio/mpeg"){
                    $image=$file_data['file_name'];
                    $fileType="audio";
                }
                else if ($file_data["file_type"] == "video/mp4" || $file_data["file_type"] == "video/3gp" || $file_data["file_type"] == "video/3gpp" || $file_data["file_type"] == "video/*") {
                    exec("ffmpeg -i " . $file_data['full_path'] . " " . $file_data['file_path'] . $file_data['raw_name'] . ".mp4");
                    $image = $file_data['raw_name'] . '.' . 'mp4';
                    $fileType = "video";
                }
                else{
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $file_data['full_path']; //get original image
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = 600;
                    $config['height'] = 600;
                    $this->load->library('image_lib', $config);
                    if (!$this->image_lib->resize()) {
                        $response = array(
                            "status" => array(
                                "code" => REST_Controller::HTTP_BAD_REQUEST,
                                "message" => "File upload Error"
                            ),
                            "response" => $this->image_lib->display_errors()
                        );
                        $this->response($response, REST_Controller::HTTP_BAD_REQUEST);
                    }
                    $image = $file_data['file_name'];
                    $fileType = "image";
                }

            }
            $message=$image;
        }
        else{
            $fileType="text";
            $message=$this->post("message",true);
            $message=$client->asciiToUnicode($message);

        }
        $receiverType="user";
        $totalReceiver=$this->Im_group_members_Model->getTotalGroupMember($receiverId);
        if($totalReceiver>2){
            $receiverType="group";
        }



        $memberIds=$this->Im_group_members_Model->getMembersWihoutSender($receiverId,$senderId);
        $m_id=$this->Im_message_Model->insert($senderId,$receiverId,$message,$fileType,$receiverType,$date,$time,$date_time);
        $fullMessage=$this->Im_message_Model->getRecentMessage($receiverId);
        $senderInfo=$this->User_Model->get_user($senderId,null,null);
        $this->Im_group_Model->updateLastActiveDate($receiverId,$date_time);


       $ios_date_time =date_format(date_create($fullMessage->date_time),DATE_ISO8601);

        $fullMessage->ios_date_time=$ios_date_time;

        $socketData=array(
            "_r"=>$headers["Authorizationkeyfortoken"],
            "to"=>$receiverId,
            "receiversId"=>$memberIds,
            "message"=>$fullMessage,
            "sender"=>$senderInfo

        );
        $registerData=array(
            "_r"=>$headers["Authorizationkeyfortoken"]
        );
        $client = new Client(new Version2X($this->config->item('socket_url')));
        $client->initialize();
        $client->emit("register",$registerData);
        $client->emit('sendMessage', $socketData);
        //$client->emit("updateMember",$response);
        $client->close();
        $response = array(
            "status" => array(
                "code" => REST_Controller::HTTP_OK,
                "message" => "Success"
            ),
           // "response" =>$this->getNewGroupInfo($receiverId,$senderId)

        );
        $this->response($response, REST_Controller::HTTP_OK);
    }


    public function addGroupMember_post() //userId[] , groupId
    {
        $headers = apache_request_headers();
        $userId = $this->User_Model->getTokenToId($headers["Authorizationkeyfortoken"]);
        $this->form_validation->set_rules('userId[]', 'userId[]', 'required');
        $this->form_validation->set_rules('groupId', 'groupId', 'required');

        if ($this->form_validation->run() == FALSE) {
            $response = array(
                "status" => array(
                    "code" => REST_Controller::HTTP_NOT_ACCEPTABLE,
                    "message" => "Validation Error"
                ),
                "response" => validation_errors()
            );
            $this->response($response, REST_Controller::HTTP_NOT_ACCEPTABLE);
            return;
        }
        $memberIds=$this->post("userId");
        $groupId=$this->post("groupId");


        foreach ($memberIds as $memberId){
            if(!$this->Im_group_members_Model->ifExist($groupId,$memberId)){
                $this->Im_group_members_Model->insert($groupId,$memberId);
            }

        }

       // $memberList=$this->Im_group_members_Model->getMembers($groupId);
        $membersInfo=array();
        $members=$this->Im_group_members_Model->getMembersWihoutSender($groupId,$userId);
        foreach ($members as $u_id){
            $membersInfo[]=$this->User_Model->get_user($u_id->u_id,null,null);
        }

        $creatorId=$this->Im_group_Model->get($groupId)->createdBy;
        $meCreator=false;
        if($userId==$creatorId){
            $meCreator=true;
        }
        $registerData=array(
            "_r"=>$headers["Authorizationkeyfortoken"]
        );

        $memberList=$this->Im_group_members_Model->getMembers($groupId);
        $updateData=array(
          "_r"=>$headers["Authorizationkeyfortoken"],
            "groupId"=>$groupId,
          "memberIds"=>$memberList
        );
        $client = new Client(new Version2X($this->config->item('socket_url')));
        $client->initialize();
        $client->emit("register",$registerData);
        $client->emit("updateMember",$updateData);
        $client->close();
        $response=array(
            "status" => array(
                "code" => REST_Controller::HTTP_OK,
                "message" => "Success"
            ),
            "response"=>array(
                "meCreator"=>$meCreator,
                "memberList"=>$membersInfo
            )

        );
        $this->response($response, REST_Controller::HTTP_OK);
    }

    private function getUrlData($url){
        $content = file_get_contents($url);
        return $content;
    }

    private function fetchUrl($string){
         preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $string, $match);

         return $match[0];
     }
}