<?php
class Im_receiver_Model extends CI_Model{

    public function update($r_id,$g_id,$m_id){
        /*$update=array(
            "received"=>1
        );*/
        $this->db->where("g_id",$g_id);
        $this->db->where("r_id",$r_id);
        $this->db->where("m_id",$m_id);
        $this->db->delete("im_receiver"/*,$update*/);

    }

    public function isExsist($m_id,$r_id,$g_id){
        $this->db->where("m_id",$m_id);
        $this->db->where("r_id",$r_id);
        $this->db->where("g_id",$g_id);
        $this->db->from('im_receiver');
        if ($this->db->count_all_results() == 0) {
            return false;
        } else {
            return true;
        }
    }
    public function isNotReceived($r_id,$g_id,$m_id){
        $this->db->where("g_id",$g_id);
        $this->db->where("r_id",$r_id);
        $this->db->where("m_id",$m_id);
        $this->db->where("received",0);
        $this->db->from('im_receiver');
        if ($this->db->count_all_results() == 0) {
            return false;
        } else {
            return true;
        }
    }
    public function DeleteAll($g_id,$r_id){
        $this->db->where("g_id",$g_id);
        $this->db->where("r_id",$r_id);
        return $this->db->delete("im_receiver");
    }
}