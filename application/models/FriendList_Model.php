<?php

class FriendList_Model extends CI_Model{
    public $userId;
    public $friendId;

    public function __construct()
    {
        parent::__construct();
    }
    public function insert($userid,$friendId){
        $this->userId=$userid;
        $this->friendId=$friendId;
        $this->db->insert("friend_list",$this);
    }
    public function delete($userId,$friendId){
        $this->db->where("userId",$userId);
        $this->db->where("friendId",$friendId);
        $this->db->delete("friend_list");
    }

    public function getList($userId){
        $this->db->select("friendId");
        $this->db->where("userId",$userId);
        $query=$this->db->get("friend_list");
        return $query->result();
    }
    function friendExist($userId,$friendId)
    {
        $this->db->where("userId",$userId);
        $this->db->where("friendId",$friendId);
        $this->db->from('friend_list');
        if ($this->db->count_all_results() == 0) {
            return false;
        } else {
            return true;
        }
    }
}