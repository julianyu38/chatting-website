<script type="text/javascript" src="<?php echo base_url("assets/newTheme/assets/js/loadingoverlay.js") ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/newTheme/assets/js/loadingoverlay_progress.js") ?>"></script>

<script type="text/javascript">
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-left",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    $(document).ready(function () {
        $("body").css({"overflow-y":"hidden"});

        /*
            window and element resizing besed on user window size
        */
        $(window).bind("resize",function () {
            let viewHeight=$(window).height();
            let viewWidth=$(window).width();
            if(viewWidth<990){
                $('#convStart').css("height",75);
                $('.persons').css({"margin-top":0});
                $(".rightSection").css({'margin-top': '16px'});
                $(".groupNameDiv").css({"padding-bottom":'32px'});
                $('.video').css({'margin-left': '-34px'});

            }
            else {
                $(".rightSection").css({'margin-top': '0px'});
                $(".groupNameDiv").css({"padding-bottom":'21px'});
                $('.video').css({'margin-left': '0px'})
            }
            /*if(viewHeight<776){
             $("#newMModalBody").css("margin-bottom", "155px");
             }else {
             $("#newMModalBody").css("margin-bottom", "160px");
             }*/
            if(viewWidth<990){
                $(".leftSection").css({"height":(viewHeight-95)});
                $(".middleSection").css({"height":(viewHeight-95)});
                $(".rightSection").css({"height":(viewHeight-95)});
            }
            else{
                $(".leftSection").css({"height":540});
                $(".middleSection").css({"height":540});
                $(".rightSection").css({"height":540});
            }
            $(".chat").css({"height":(viewHeight-220)});
            $('.persons').css({"height":(viewHeight-160)});
            $('.personsList').css({"height":(viewHeight-250)});
        }).trigger("resize");

        /*
           --------Global variables
         */
        let chatBox=$('#chatBox');
        let videoObjects=[];
        let responce=localStorage.getItem("_r");
        let type=jwt_decode(responce).userType;
        let start=0;
        let limit=10;
        let totalRetivedMessage=0;
        let userId=jwt_decode(responce).userId;
        let groupIds=[];
        let time=[];
        let groupImages={};
        let newMemberInput=null;
        let scrollPosition=null;
        let notRequested=true;
        let addmember=null;
        var typing = false;
        var typingTimeout = undefined;
        // --------- Global Functions--------------

        function typingTimeoutFunction(){
            let groupId=$("#addMember").attr('data-group');
            let data={
                _r:token,
                groupId:groupId
            };
            typing = false;
            socket.emit("notTyping",JSON.stringify(data));
        }
        function onKeyDownNotEnter(){
            let groupId=$("#addMember").attr('data-group');
            let data={
                _r:token,
                groupId:groupId
            };
            if(typing == false) {
                typing = true;
                socket.emit("typing",JSON.stringify(data));
                typingTimeout = setTimeout(typingTimeoutFunction, 1000);
            } else {
                clearTimeout(typingTimeout);
                typingTimeout = setTimeout(typingTimeoutFunction, 1000);
            }

        }
        // this function used to clear new message div
        function resetNewMessage () {
            $("#newMessageFile").replaceWith($("#newMessageFile").val('').clone(true));
            $('#newMessagefileIV').attr("src","<?php echo base_url('assets/img/i-camera.png')?>");

            $('.twemoji-textarea').html("");
            $('.twemoji-textarea-duplicate').html("");
            $('#newMessageText').text("");
            $('#newMessageText').val("");
            $('.close').trigger("click");

        }

        // this function used to clear message div
        function reset () {
            $("#messageFile").replaceWith($("#messageFile").val('').clone(true));
            $('#fileIV').attr("src","<?php echo base_url('assets/img/i-camera.png')?>");

            $('.twemoji-textarea').html("");
            $('.twemoji-textarea-duplicate').html("");
            $('#message').text("");
            $('#message').val("");

        }

        // function for checking image/video type and size before uploading
        function imageChange(event) {
            let file = this.files[0];
            let imagefile = file.type;
            let size=file.size;
            let match= ["image/jpeg","image/png","image/jpg","video/3gpp","video/mp4","video/3gp","audio/mp3"];
            if(size>20971520){
                toastr.error("Max limit 20Mb exceeded");
                return ;
            }

            if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]) || (imagefile==match[3]) || (imagefile==match[4]) || (imagefile==match[5])||(imagefile==match[6])))
            {
                toastr.error("This type of file is not allowed");
                return false;
            }else {
                $('#sendMessage').trigger('click');
                /* let type=null;
                 let url=URL.createObjectURL(this.files[0]);
                 if((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])){
                 type=new Image();
                 type.src=url;
                 type.onload = function() {
                 captureImage(type);
                 };

                 }
                 else{
                 type = document.createElement('video');
                 let source = document.createElement('source');
                 source.setAttribute('src',url);
                 type.appendChild(source);
                 type.muted = true;
                 type.play();
                 setTimeout(function(){
                 type.pause(); // note the [0] to access the native element
                 captureImage(type);
                 }, 3000);

                 }*/

            }
        }

        // function for checking image/video type and size before uploading
        function imageChangeNewMessage(event) {
            let file = this.files[0];
            let imagefile = file.type;
            let size=file.size;
            let match= ["image/jpeg","image/png","image/jpg","video/3gpp","video/mp4","video/3gp","audio/mp3"];
            if(size>20971520){
                toastr.error("Max limit 20Mb exceeded");
                return ;
            }

            if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]) || (imagefile==match[3]) || (imagefile==match[4]) || (imagefile==match[5]) || (imagefile==match[6])))
            {
                toastr.error("This type of file is not allowed");
                return false;
            }else {

                $('#newSendMessage').trigger('click');
                /*let type=null;
                 let url=URL.createObjectURL(this.files[0]);
                 if((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])){
                 type=new Image();
                 type.src=url;
                 type.onload = function() {
                 captureImagenewMessage(type);
                 };

                 }
                 else{
                 type = document.createElement('video');
                 let source = document.createElement('source');
                 source.setAttribute('src',url);
                 type.appendChild(source);
                 type.muted = true;
                 type.play();
                 setTimeout(function(){
                 type.pause(); // note the [0] to access the native element
                 captureImagenewMessage(type);
                 }, 3000);

                 }*/

            }
        }

        // Api pagination functions
        function increaseStart() {
            start+=limit;
        }
        function resetStart() {
            start=0;
        }
        function resetRetiveMessage(){
            totalRetivedMessage=0;
        }

        // this function prints group list on the left side
        function printGroupList(groups){
            let html="";
            groupIds=[];

            time={};
            for(i=0;i<groups.length;i++){
                groupIds.push(groups[i].groupId);
                time[groups[i].groupId]=groups[i].lastActive;
                html += " <li class=\"person\" data-chat=\"person1\" id='group_"+groups[i].groupId+"' data-group=\""+groups[i].groupId+"\">";
                for (j=0;j<groups[i].groupImage.length;j++){
                    groupImages[groups[i].groupId]=groups[i].groupImage;
                    html += "                        <img class=\"img-responsive img-circle\" style=\"width: 40px; height: 40px;border-radius: 50%\" src=\""+groups[i].groupImage[j]+"\" >";
                }
                html += "                        <span class=\"name\" style=\"overflow: hidden\"><div>"+groups[i].groupName+"</div><\/span>";
                let date=moment(groups[i].lastActive,moment.ISO_8601).fromNow();

                html += "                        <span id='time_"+groups[i].groupId+"' class=\"time\">"+date+"<\/span>";
                if(groups[i].messageType==="text"){
                    let recentMessage=groups[i].recentMessage;
                    if(recentMessage===null){
                        recentMessage='';
                    }
                    html += "                        <span style='float: left' id='messageType_"+groups[i].groupId+"' class=\"preview\">"+recentMessage+"<\/span>";
                }else{
                    let messageType=groups[i].messageType;
                    if(messageType===null){
                        messageType='';
                    }
                    html += "                        <span style='float: left' id='messageType_"+groups[i].groupId+"' class=\"preview\">"+messageType+"<\/span>";
                }
                html += "                        <div style='width: 26px;margin-left: 90%;' id='notice_"+groups[i].groupId+"' class=\"pad-2 notice hidden text-center\" ><\/div>";
                html += "                    <\/li>";
            }
            $("#groups").html(html);
        }

        //This function is used to get the group list
        function getGroupList(callback) {
            let settings = {
                "async": true,
                "crossDomain": true,
                "url": "<?php echo base_url('imApi/getGroups/') ?>",
                "method": "GET",
                "headers": {
                    "authorization": "Basic YWRtaW46MTIzNA==",
                    "Authorizationkeyfortoken": String(responce),
                    "cache-control": "no-cache",
                    "postman-token": "eb27c011-391a-0b70-37c5-609bcd1d7b6d"
                },
                "processData": false,
                "contentType": false,
                "statusCode": {
                    401: function(error) {
                       location.href="<?php echo base_url('userview/logout') ?>"
                    }
                }
            };

            $.ajax(settings).done(function (response) {

                let groups=response.response;

                if(groups.length<=0){
                    $('#addMember').attr('data-group',null);
                    $('#addMember').addClass("hidden");
                    chatBox.html('<img id="blankImg" src="<?php echo base_url('assets/img/nomess.png')?>" class="img-responsive blankImg" style="width:500px;margin-top: 20px;">');
                    chatBox.addClass("text-center");
                    $('#groupMembers').html("");
                    $('#groups').html('');
                    $("#editGroupName").addClass("hidden");
                    $('.UserNames').html('');
                }else{
                    $('#addMember').removeClass("hidden");
                    $("#editGroupName").removeClass("hidden");
                    printGroupList(groups);
                    // $("#groups li").first().trigger("click");
                    if(callback!=null|| callback!=""){
                        if(groups.length>0){
                            callback(true);
                        }else {
                            callback(false);
                        }

                    }else {
                        $("#groups li").first().trigger("click",[{update:true}]);
                    }
                }



            });

        }

        //this function is used to print the group member list on the right side
        function printGroupMembers(members,meCreator,groupId) {
            let html="";
            for (i=0;i<members.length;i++){
                html += "<li class=\"person\"  style=\"padding-top: 5px;padding-bottom: 22px;cursor: default;\">";
                html += "                        <img src=\""+members[i].profilePictureUrl+"\" alt=\"\" \/>";
                html += "                        <span  class=\"name\"><div style='margin-top: 8px'>"+members[i].firstName+" "+members[i].lastName +"</div><\/span>";
                if(meCreator){
                    html += "                        <span class=\"time\" style='padding-top: 5px' ><a href=\"#\" data-group=\""+groupId+"\" data-member=\""+members[i].userId+"\" class=\"btn-danger btn-extra-small btnMemberDelete\"><i class=\"fa fa-trash\"><\/i><\/a><\/span>";
                }
                html += "                    <\/li>";
            }
            $('#groupMembers').html(html);
        }

        //This function is used to get the group member list
        function getGroupMembers(groupId) {
            let settings = {
                "async": true,
                "crossDomain": true,
                "url": "<?php echo base_url('imApi/getMembers?groupId=') ?>"+groupId,
                "method": "GET",
                "headers": {
                    "authorization": "Basic YWRtaW46MTIzNA==",
                    "Authorizationkeyfortoken": String(responce),
                    "cache-control": "no-cache",
                    "postman-token": "eb27c011-391a-0b70-37c5-609bcd1d7b6d"
                },
                "processData": false,
                "contentType": false
            };
            $.ajax(settings).done(function (response) {
                let members=response.response.memberList;
                let meCreator=response.response.meCreator;
                printGroupMembers(members,meCreator,groupId);
            });

        }

        //This function is used to print the group name and three member image on the right side top
        function printGroupInfo(groupId,groupImages,groupName){
            let html="";
            let images=groupImages[groupId];
            for(i=0;i<images.length;i++){
                html += "<img class=\"img-responsive img-circle\" style=\"width: 40px; height: 40px;border-radius: 50%\" src=\""+images[i]+"\" >";
            }
            $('.rightGroupImages').html(html);
            $('.be-use-name').html(groupName);
            $clamp($('.be-use-name')[0], { clamp: 2, useNativeClamp: false });
        }

        function clampData() {
            $('.clamp-desc').each(function (index,element) {
                $clamp(element, {clamp: 4, useNativeClamp: false});
            });
            $('.clamp-title').each(function (index,element) {
                $clamp(element, {clamp: 3, useNativeClamp: false});
            });
        }
        //This function is used to  get frind list of user
        function getMembers(callback) {   // get friends list
            let settings={
                "async": true,
                "crossDomain": true,
                "url": "<?php echo base_url('user/friendList/') ?>",
                "method": "GET",
                "headers": {
                    "authorization": "Basic YWRtaW46MTIzNA==",
                    "Authorizationkeyfortoken": String(responce),
                    "cache-control": "no-cache",
                    "postman-token": "eb27c011-391a-0b70-37c5-609bcd1d7b6d"
                },
                "dataType" : 'json'
            };
            $.ajax(settings).done(function (response) {

                let data=response.response;
                callback(data);
            });
        }

        //This function is used to clear the current chat box for retrieving new message for the new group
        function clearChatBox() {
            chatBox.html('');
        }

        //This function is used to create the preview for a link sheared in message
        function getLinkPreview(linkData,link){
            if(linkData.playerOrImageUrl.type==='player'){
                return "<div class=''><iframe src='"+linkData.playerOrImageUrl.url+"' class='medea-frame iframe-wrapper' allowfullscreen></iframe></div>";
            }
            else {
                let image = "<img src='<?php echo base_url("/assets/img/compact_camera1600.png") ?>' id='tImg_blank' width='100%'>";
                if(linkData.playerOrImageUrl.url!=null){
                     image = "<img src='" + linkData.playerOrImageUrl.url + "' id='tImg' width='100%'>";
                    return "<div class='linkPreview-wrapper'>" +
                        "<a href='" + link + "' target=\"_blank\">" +
                        "<div id='texts'>" +
                        "<div id='thumbnail' >" + image +
                        "</div> " +
                        "<div id='desc'>" +
                        "<div id='title'>" +
                        "<div class='clamp-title'>" + linkData.title +
                        "</div>" +
                        "</div>" +
                        "<div class='clamp-desc'>" + linkData.description +
                        "</div> " +
                        "<div id='meta'>" +
                        "<div id='domain'>" + linkData.host +
                        "</div>" +
                        "<div class='clear'></div>" +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "</a>" +
                        "</div>";
                }
                return "<div class='linkPreview-wrapper'>" +
                    "<a href='" + link + "' target=\"_blank\">" +
                    "<div id='texts'>" +
                    "<div id='thumbnail' >" + image +
                    "</div> " +
                    "<div id='desc'>" +
                    "<div id='title'>" +
                    "<div class='clamp-title'>" + linkData.title +
                    "</div>" +
                    "</div> " +
                    "<div class='clamp-desc'>" + linkData.description +
                    "</div> " +
                    "<div id='meta'>" +
                    "<div id='domain'>" + linkData.host +
                    "</div>" +
                    "<div class='clear'></div>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</a>" +
                    "</div>";
            }
        }

        //This function is used to format the links and add the emojis send by user
        function parseMessage(message) {
            return  twemoji.parse(
                anchorme(message,{
                    truncate:[15,10],
                    attributes:[
                        function(urlObj){
                            if(urlObj.protocol !== "mailto:")
                                return {name:"target",value:"blank"};
                        }
                    ]
                })
            );
        }

        //This function is used to retrieve messages from server based on group id
        function getMessage(groupId) {
            if(start==1){
                start=0;
            }
            let url="<?php echo base_url('imApi/getMessage?groupId=') ?>"+groupId+"&limit="+limit+"&start="+start;

            let settings={
                "async": true,
                "crossDomain": true,
                "url": url,
                "method": "GET",
                "headers": {
                    "authorization": "Basic YWRtaW46MTIzNA==",
                    "Authorizationkeyfortoken": String(responce),
                    "cache-control": "no-cache",
                    "postman-token": "eb27c011-391a-0b70-37c5-609bcd1d7b6d"
                },
                "processData": false,
                "contentType": false

            };
            $.ajax(settings).done(function (result) {

                let data=result.response;
                let html="";
                totalRetivedMessage+=data.length;

                if(data.length==0){
                    chatBox.html('<img id="blankImg" src="<?php echo base_url('assets/img/nomess.png')?>" class="img-responsive blankImg" style="width:500px;margin-top: 20px;">');
                    chatBox.addClass("text-center");
                }else{
                    chatBox.removeClass("text-center");
                    for(i=0;i<data.length;i++){

                        let sender=data[i].sender;
                        let message=data[i].message;

                        let senderId=data[i].sender.userId;
                        if(senderId==userId){
                            html += "<div class=\"fw-im-message  fw-im-isme fw-im-othersender\" data-og-container=\"\">";
                            if(message.type=="text"){
                                html += "                    <div id='message_"+message.m_id+"' class=\"fw-im-message-text\">"+parseMessage(message.message)+"<\/div>";
                                if(message.linkData!=null){
                                    html+=getLinkPreview(JSON.parse(message.linkData),message.link);
                                }
                            }
                            if(message.type=="image"){
                                html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\"><a style=\"width: 200px;height: 200px\" href=\""+message.message+"\" class=\"ol-hover hover-5 ol-lightbox\"><img height=\"200px\" width=\"200px\" src=\""+message.message+"\" alt=\"image hover\">";
                                html += "                            <div class=\"ol-overlay ov-light-alpha-80\"><\/div>";
                                html += "                            <div class=\"icons\"><i class=\"fa fa-camera\"><\/i><\/div><\/a>";
                                html += "                            <\/div>";
                            }
                            if(message.type=="video"){
                                html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\" >";
                                html += "                        <video id='video_"+message.m_id+"' class='video-js ' width=\"250px\" height=\"150px\" controls=\"true\" preload='none' name=\"media\"><source src=\""+message.message+"\" type=\"video\/mp4\"><\/video>";
                                html += "                    <\/div>";
                            }
                            if(message.type=="audio"){
                                html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\" >";
                                html += "                        <audio id='audio_"+message.m_id+"' class='video-js  vjs-default-skin' width=\"250px\" height=\"150px\" controls=\"true\" preload='none' name=\"media\"><source src=\""+message.message+"\" type=\"audio\/mp3\"><\/audio>";
                                html += "                    <\/div>";
                            }
                            html += "                    <div class=\"fw-im-message-author\"  title=\"" + sender.firstName + " " + sender.lastName + "\">";
                            html += "                        <img src=\""+sender.profilePictureUrl+"\" >";
                            html += "                    <\/div>";
                            html += "                    <div class=\"fw-im-message-time\">";
                            html += "                        <span style=\"cursor:help\" title=\""+moment(message.ios_date_time,moment.ISO_8601).format('LLLL')+"\">"+moment(message.ios_date_time,moment.ISO_8601).calendar()+"<\/span>";
                            html += "                    <\/div>";
                            html += "                <\/div>";
                        }
                        else{
                            html += "                <div class=\"fw-im-message  fw-im-isnotme fw-im-othersender\" data-og-container=\"\">";
                            if(message.type=="text"){
                                html += "                    <div id='message_"+message.m_id+"' class=\"fw-im-message-text\">"+parseMessage(message.message)+"<\/div>";
                                if(message.linkData!=null){

                                    html+=getLinkPreview(JSON.parse(message.linkData),message.link);
                                }
                            }
                            if(message.type=="image"){
                                html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\"><a style=\"width: 200px;height: 200px\" href=\""+message.message+"\" class=\"ol-hover hover-5 ol-lightbox\"><img height=\"200px\" width=\"200px\" src=\""+message.message+"\" alt=\"image hover\">";
                                html += "                            <div class=\"ol-overlay ov-light-alpha-80\"><\/div>";
                                html += "                            <div class=\"icons\"><i class=\"fa fa-camera\"><\/i><\/div><\/a>";
                                html += "                            <\/div>";
                            }
                            if(message.type=="video"){
                                html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\">";
                                html += "                        <video id='video_"+message.m_id+"' class='video-js' width=\"250px\" height=\"150px\" controls=\"true\" preload='none'  name=\"media\"><source src=\""+message.message+"\" type=\"video\/mp4\"><\/video>";
                                html += "                    <\/div>";
                            }
                            if(message.type=="audio"){
                                html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\" >";
                                html += "                        <audio id='audio_"+message.m_id+"' class='video-js  vjs-default-skin' width=\"250px\" height=\"150px\" controls=\"true\" preload='none' name=\"media\"><source src=\""+message.message+"\" type=\"audio\/mp3\"><\/audio>";
                                html += "                    <\/div>";
                            }
                            html += "                    <div class=\"fw-im-message-author\"  title=\"" + sender.firstName + " " + sender.lastName + "\">";
                            html += "                        <img src=\""+sender.profilePictureUrl+"\" >";
                            html += "                    <\/div>";
                            html += "                    <div class=\"fw-im-message-time\">";
                            html += "                        <span style=\"cursor:help\" title=\""+moment(message.ios_date_time,moment.ISO_8601).format('LLLL')+"\">"+moment(message.ios_date_time,moment.ISO_8601).calendar()+"<\/span>";
                            html += "                    <\/div>";
                            html += "                <\/div>";
                        }


                    }

                    chatBox.html("");

                    chatBox.append(html);
                    chatBox.scrollTop(0);

                    for(i=0;i<data.length;i++){
                        let allMessage=data[i].message;
                        if(allMessage.type=="video"){
                            videoObjects.push(videojs("video_"+allMessage.m_id, {}, function(){
                                    // Player (this) is initialized and ready.
                                })
                            );
                        }else if(allMessage.type=="audio"){
                            videoObjects.push(videojs("audio_"+allMessage.m_id, {}, function(){
                                    // Player (this) is initialized and ready.
                                })
                            );
                        }
                    }

                    let height=chatBox[0].scrollHeight;
                    scrollPosition=height;
                    //chatBox.scrollTop( chatBox.prop( "scrollHeight" ) );
                    chatBox.scrollTop(height);

                    $('#notice_'+groupId).addClass("hidden");
                    lightBox.init();
                    chatBox.perfectScrollbar();
                    clampData();
                }


            });

        }

        //This function is used to send message to the server
        function sendMessage(form,sendFile,newmessage) {
            let settings=null;
            if(sendFile){
                let progress1 = new LoadingOverlayProgress();

                settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": "<?php echo base_url('imApi/sendMessage') ?>",
                    "method": "POST",
                    "headers": {
                        "authorization": "Basic YWRtaW46MTIzNA==",
                        "Authorizationkeyfortoken": String(responce),
                        "cache-control": "no-cache",
                        "postman-token": "58e7510b-ad46-6037-fc4d-028915069e2b"
                    },
                    "xhr": function() {

                        let xhr = new window.XMLHttpRequest();

                        xhr.upload.addEventListener("progress", function(evt) {
                            if(sendFile) {
                                let percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);

                                if (percentComplete >= 100) {
                                    //clearInterval(iid1);
                                    delete progress1;
                                    $("body").LoadingOverlay("hide");
                                    return;
                                }
                                progress1.Update(percentComplete);
                            }
                        }, false);

                        return xhr;
                    },
                    "processData": false,
                    "contentType": false,
                    "mimeType": "multipart/form-data",
                    "data": form,
                    "error":function () {
                        toastr.error("An error occurred. Please try again");
                    },
                    "beforeSend":function () {
                        $('.close').trigger("click");
                        if(sendFile){
                            $("body").LoadingOverlay("show", {
                                custom  : progress1.Init()
                            });
                        }

                    },
                    "complete":function () {
                        delete progress1;
                        $("body").LoadingOverlay("hide");
                    }
                };
            }else{

                settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": "<?php echo base_url('imApi/sendMessage') ?>",
                    "method": "POST",
                    "headers": {
                        "authorization": "Basic YWRtaW46MTIzNA==",
                        "Authorizationkeyfortoken": String(responce),
                        "cache-control": "no-cache",
                        "postman-token": "58e7510b-ad46-6037-fc4d-028915069e2b"
                    },
                    "processData": false,
                    "contentType": false,
                    "mimeType": "multipart/form-data",
                    "data": form,
                    "error":function () {
                        toastr.error("An error occurred. Please try again");
                    },
                    "beforeSend":function () {
                        $('.close').trigger("click");

                    },
                    "complete":function () {

                    }
                };
            }

            $.ajax(settings).done(function (res) {

                resetNewMessage();
                if(newmessage){
                    let currentGroupId=$('#addMember').attr("data-group");
                    getGroupList(function (ret) {
                        if(ret){
                            $('.person').removeClass('active');
                            $('#groups li').first().trigger("click",[{update:true}]);
                        }
                    });
                }

            });
        }

        // unused function. have a plan used in the future
        function captureImage(file) {
            let canvas = document.createElement("canvas");
            canvas.width = 40;
            canvas.height = 40;

            canvas.strokeStyle = 'black';
            canvas.lineWidth = 1;
            canvas.getContext('2d').strokeRect(0, 0, canvas.width, canvas.height);
            canvas.getContext('2d').drawImage(file, 0, 0, canvas.width-1, canvas.height-1);

            let img = document.getElementById("fileIV");
            img.src = canvas.toDataURL("image/png");
            //$output.prepend(img);
        };
        function captureImagenewMessage(file) {
            let canvas = document.createElement("canvas");
            canvas.width = 40;
            canvas.height = 40;

            canvas.strokeStyle = 'black';
            canvas.lineWidth = 1;
            canvas.getContext('2d').strokeRect(0, 0, canvas.width, canvas.height);
            canvas.getContext('2d').drawImage(file, 0, 0, canvas.width-1, canvas.height-1);

            let img = document.getElementById("newMessagefileIV");
            img.src = canvas.toDataURL("image/png");
            //$output.prepend(img);
        };

        //This function is used to update the current group list shown in the right side
        function updateGroupList () {

            let currentGroupId = $('#addMember').attr("data-group");
            getGroupList(function (ret) {
                if (ret) {
                    $('.person').removeClass('active');
                    if (document.getElementById('group_' + currentGroupId)) {
                        $('#group_' + currentGroupId).addClass('active');
                    } else {
                        $('#groups li').first().trigger("click",[{update:true}]);
                    }
                }else {

                }
            });
        }

        //update the message time on the left side
        function updateTime() {
            for (i=0;i<groupIds.length;i++){
                let date=moment(time[groupIds[i]],moment.ISO_8601).fromNow();
                $('#time_'+groupIds[i]).html(date);
            }

        }


// -----------------End of Global functions --------------------------//


        $('#groups').perfectScrollbar('update');
        $('#groupMembers').perfectScrollbar();
        chatBox.perfectScrollbar();
        chatBox.on("ps-scroll-up",function() {

            if (notRequested && chatBox.scrollTop()==0) {
                notRequested=false;
                increaseStart();

                let groupId= $('#addMember').attr('data-group');
                let url="<?php echo base_url('imApi/getMessage?groupId=') ?>" + groupId + "&limit="+limit+"&start=" + start;


                let settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": url,
                    "method": "GET",
                    "headers": {
                        "authorization": "Basic YWRtaW46MTIzNA==",
                        "Authorizationkeyfortoken": String(responce),
                        "cache-control": "no-cache",
                        "postman-token": "eb27c011-391a-0b70-37c5-609bcd1d7b6d"
                    },
                    "processData": false,
                    "contentType": false,
                    "beforeSend":function () {
                        chatBox.prepend("<div class='loader'></div>");
                    },
                    "complete":function () {
                        $('.loader').hide();

                    }
                };
                $.ajax(settings).done(function (result) {
                    notRequested=true;
                    let data = result.response;

                    if(totalRetivedMessage==result.totalMessage){
                        resetStart();
                        return;
                    }
                    let html = "";
                    for (i = 0; i < data.length; i++) {
                        let self = data[i].self;
                        let sender = data[i].sender;
                        let message = data[i].message;

                        let senderId=data[i].sender.userId;
                        if(parseInt(senderId)==parseInt(userId)){
                            html += "<div  class=\"fw-im-message  fw-im-isme fw-im-othersender\" data-og-container=\"\">";
                            if (message.type == "text") {
                                html += "                    <div id='message_"+message.m_id+"' class=\"fw-im-message-text\">" +parseMessage(message.message) + "<\/div>";
                                if(message.linkData!=null){
                                    html+=getLinkPreview(JSON.parse(message.linkData),message.link);
                                }
                            }
                            if (message.type == "image") {
                                html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\"><a style=\"width: 200px;height: 200px\" href=\"" + message.message + "\" class=\"ol-hover hover-5 ol-lightbox\"><img height=\"200px\" width=\"200px\" src=\"" + message.message + "\" alt=\"image hover\">";
                                html += "                            <div class=\"ol-overlay ov-light-alpha-80\"><\/div>";
                                html += "                            <div class=\"icons\"><i class=\"fa fa-camera\"><\/i><\/div><\/a>";
                                html += "                            <\/div>";
                            }
                            if (message.type == "video") {
                                html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\" >";
                                html += "                        <video id='video_"+message.m_id+"' class='video-js ' width=\"250px\" height=\"150px\" controls=\"true\" preload='none'  name=\"media\"><source src=\"" + message.message + "\" type=\"video\/mp4\"><\/video>";
                                html += "                    <\/div>";
                            }
                            if (message.type == "audio") {
                                html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\" >";
                                html += "                        <audio id='audio_"+message.m_id+"' class='video-js ' width=\"250px\" height=\"150px\" controls=\"true\" preload='none' name=\"media\"><source src=\"" + message.message + "\" type=\"audio\/mp3\"><\/audio>";
                                html += "                    <\/div>";
                            }

                            html += "                    <div class=\"fw-im-message-author\"   title=\"" + sender.firstName + " " + sender.lastName + "\">";
                            html += "                        <img src=\""+sender.profilePictureUrl+"\" >";
                            html += "                    <\/div>";
                            html += "                    <div class=\"fw-im-message-time\">";
                            html += "                        <span style=\"cursor:help\" title=\"" + moment(message.ios_date_time,moment.ISO_8601).format('LLLL') + "\">" + moment(message.ios_date_time,moment.ISO_8601).calendar() + "<\/span>";
                            html += "                    <\/div>";
                            html += "                <\/div>";
                        }
                        else {
                            html += "                <div class=\"fw-im-message  fw-im-isnotme fw-im-othersender\" data-og-container=\"\">";
                            if (message.type == "text") {
                                html += "                    <div id='message_"+message.m_id+"' class=\"fw-im-message-text\">" + parseMessage(message.message) + "<\/div>";
                                if(message.linkData!=null){
                                    html+=getLinkPreview(JSON.parse(message.linkData),message.link);
                                }
                            }
                            if (message.type == "image") {
                                html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\"><a style=\"width: 200px;height: 200px\" href=\"" + message.message + "\" class=\"ol-hover hover-5 ol-lightbox\"><img height=\"200px\" width=\"200px\" src=\"" + message.message + "\" alt=\"image hover\">";
                                html += "                            <div class=\"ol-overlay ov-light-alpha-80\"><\/div>";
                                html += "                            <div class=\"icons\"><i class=\"fa fa-camera\"><\/i><\/div><\/a>";
                                html += "                            <\/div>";
                            }
                            if (message.type == "video") {
                                html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\">";
                                html += "                        <video id='video_"+message.m_id+"' class='video-js ' width=\"250px\" height=\"150px\" controls=\"true\"  preload='none' name=\"media\"><source src=\"" + message.message + "\" type=\"video\/mp4\"><\/video>";
                                html += "                    <\/div>";
                            }
                            if (message.type == "audio") {
                                html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\" >";
                                html += "                        <audio id='audio_"+message.m_id+"' class='video-js ' width=\"250px\" height=\"150px\" controls=\"true\" preload='none'  name=\"media\"><source src=\"" + message.message + "\" type=\"audio\/mp3\"><\/audio>";
                                html += "                    <\/div>";
                            }
                            html += "                    <div class=\"fw-im-message-author\"  title=\"" + sender.firstName + " " + sender.lastName + "\">";
                            html += "                        <img src=\""+sender.profilePictureUrl+"\" >";
                            html += "                    <\/div>";
                            html += "                    <div class=\"fw-im-message-time\">";
                            html += "                        <span style=\"cursor:help\" title=\"" + moment(message.ios_date_time,moment.ISO_8601).format('LLLL') + "\">" + moment(message.ios_date_time,moment.ISO_8601).calendar() + "<\/span>";
                            html += "                    <\/div>";
                            html += "                <\/div>";
                        }

                    }
                    totalRetivedMessage+=data.length;

                    chatBox.prepend(html);
                    for(i=0;i<data.length;i++){
                        let allMessage=data[i].message;
                        if(allMessage.type=="video"){
                            videoObjects.push(videojs("video_"+allMessage.m_id, {}, function(){
                                    // Player (this) is initialized and ready.
                                })
                            );
                        }else if(allMessage.type=="audio"){
                            videoObjects.push(videojs("audio_"+allMessage.m_id, {}, function(){
                                    // Player (this) is initialized and ready.
                                })
                            );
                        }
                    }
                    /*if(data.length>0){
                     let m_id=data[data.length-1].message.m_id;
                     chatBox.animate({scrollTop:$("#message_"+m_id).offset().top},3);
                     }*/
                    // let height=chatBox[0].scrollHeight;

                    chatBox.scrollTop(scrollPosition);
                    lightBox.init();
                    $('.loader').hide();
                    clampData();

                });
            }
        });
        //$(".rightSection").perfectScrollbar();

        $('#newMessageText').twemojiPicker({
            init: "Your message.....",
            size: '30px',
            icon: 'grinning',
            iconSize: '25px',
            height: '90px',
            width: '100%',
            border:'0',
            category: ['smile', 'cherry-blossom', 'video-game', 'oncoming-automobile', 'symbols'],
            categorySize: '20px',
            pickerPosition: 'bottom',
            pickerHeight: '150px',
            pickerWidth: '100%'
        });

        let sendMessageSettings= {
            init: "Your message.....",
            size: '30px',
            icon: 'grinning',
            iconSize: '25px',
            height: '50px',
            width: '100%',
            border:'0',
            category: ['smile', 'cherry-blossom', 'video-game', 'oncoming-automobile', 'symbols'],
            categorySize: '20px',
            pickerPosition: 'top',
            pickerHeight: '150px',
            pickerWidth: '100%'
        };



        if(responce!=null && responce!='' && type==1)
        {

            getGroupList(function (data) {
                if(data){
                    let token={
                        _r:String(responce)
                    };
                    socket.emit("messageNotification",JSON.stringify(token));
                    $("#groups li").first().trigger("click",[{update:true}]);

                }
            });
        }

        else {
            location.href="<?php echo base_url("userview/logout")  ?>";
        }


        $('#message').twemojiPicker(sendMessageSettings);

        $('#groups').on("click",".person",function (e,update) {

            $('#chatBox').perfectScrollbar('destroy');
            for(i=0;i<videoObjects.length;i++){
                videoObjects[i].dispose();
            }
            videoObjects=[];
            resetStart();
            resetRetiveMessage();
            if ($(this).hasClass('active')) {
                return false;
            }
            if ($('#addMember').hasClass('hidden')) {
                $('#addMember').removeClass('hidden');
            }
            let groupId = $(this).attr('data-group');
            let personName = $(this).find('.name').text();

            $('.UserNames').html(personName);
            $('.person').removeClass('active');
            $(this).addClass('active');
            let oldGroupId=$('#addMember').attr('data-group');
            $('#addMember').attr('data-group',groupId);
            $('#editGroupName').attr('data-group',groupId);
            let updateList=true;
            if(typeof update!=='undefined' ){
                updateList=update.update;
            }
            if(updateList){
                getGroupMembers(groupId);
            }


            clearChatBox();
            getMessage(groupId,start,limit);

            reset();
            let data={groupId:groupId};
            if(oldGroupId!==null|| oldGroupId!==""){
                socket.emit("leaveRoom",oldGroupId);
            }
            printGroupInfo(groupId,groupImages,personName);

            socket.emit("joinRoom",groupId);

        });



        $('#groupMembers').on("click",".btnMemberDelete",function (e) {
            let groupId = $(this).attr('data-group');
            let memberId=$(this).attr('data-member');
            let form=new FormData();

            form.append("groupId",groupId);
            form.append("memberId",memberId);

            let settings = {
                "async": true,
                "crossDomain": true,
                "url": "<?php echo base_url('imApi/deleteMember') ?>",
                "method": "POST",
                "headers": {
                    "authorization": "Basic YWRtaW46MTIzNA==",
                    "Authorizationkeyfortoken": String(responce),
                    "cache-control": "no-cache",
                    "postman-token": "eb27c011-391a-0b70-37c5-609bcd1d7b6d"
                },
                "processData": false,
                "contentType": false,
                "data":form
            };

            $.ajax(settings).done(function (res) {
                //let data=JSON.parse(response)
                printGroupMembers(res.response.memberList,res.response.meCreator,groupId);
                toastr.success("Member deleted");
                // getGroupMembers(groupId);

            });

        });



        $('#addMember').on("click",function (e) {
            newMemberInput=null;
            getMembers(function (res) {
                let q=[];
                for(i=0;i<res.length;i++) {
                    if(res[i].userStatus!=0) {
                        let md = {
                            id: parseInt(res[i].userId),
                            name: res[i].firstName + " " + res[i].lastName,
                            picture: res[i].profilePictureUrl,
                            email: res[i].userEmail
                        };
                        q.push(md);
                    }
                }

                newMemberInput=$('#addNewMemberInput').magicSuggest({
                    placeholder: 'Search for members...',
                    maxSelection:null,
                    data: q,
                    renderer: function(data){
                        return '<div style="padding: 5px; overflow:hidden;">' +
                            '<div style="float: left;"><img style="width: 25px;height: 25px" src="' + data.picture + '" /></div>' +
                            '<div style="float: left; margin-left: 5px">' +
                            '<div style="font-weight: bold; color: #333; font-size: 12px; line-height: 11px">' + data.name + '</div>' +
                            '<div style="color: #999; font-size: 9px">' + data.email + '</div>' +
                            '</div>' +
                            '</div><div style="clear:both;"></div>'; // make sure we have closed our dom stuff
                    }
                });
                newMemberInput.clear();
                $('#addNewMemberModal').modal('show');
            });
        });

        $("#newMemberAddBtn").on("click",function (e) {
            let userIds=newMemberInput.getValue();
            let groupId= $('#addMember').attr('data-group');

            if(userIds.length>0) {
                let form = new FormData();
                for (i = 0; i < userIds.length; i++) {
                    form.append("userId[]", userIds[i]);
                }
                form.append("groupId", groupId);

                let settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": "<?php echo base_url('imApi/addGroupMember/') ?>",
                    "method": "POST",
                    "headers": {
                        "authorization": "Basic YWRtaW46MTIzNA==",
                        "Authorizationkeyfortoken": String(responce),
                        "cache-control": "no-cache",
                        "postman-token": "eb27c011-391a-0b70-37c5-609bcd1d7b6d"
                    },
                    "processData": false,
                    "contentType": false,
                    "data": form
                };
                $.ajax(settings).done(function (res) {
                    printGroupMembers(res.response.memberList, res.response.meCreator, groupId);
                    newMemberInput.clear();
                    toastr.success("member add successful");
                    $('#addNewMemberModal').modal('hide');
                    // getGroupMembers(groupId);

                });

            }
        });


        $('#newMessage').on("click",function (e) {
            resetNewMessage ();
            getMembers(function (res) {
                let q=[];
                for(i=0;i<res.length;i++) {
                    if(res[i].userStatus!=0) {
                        let md = {
                            id: parseInt(res[i].userId),
                            name: res[i].firstName + " " + res[i].lastName,
                            picture: res[i].profilePictureUrl,
                            email: res[i].userEmail
                        };
                        q.push(md);
                    }
                }
                addmember=$('#addMemberInput').magicSuggest({
                    placeholder: 'Add members...',
                    maxSelection:null,
                    data: q,
                    renderer: function(data){
                        return '<div style="padding: 5px; overflow:hidden;">' +
                            '<div style="float: left;"><img style="width: 25px;height: 25px" src="' + data.picture + '" /></div>' +
                            '<div style="float: left; margin-left: 5px">' +
                            '<div style="font-weight: bold; color: #333; font-size: 12px; line-height: 11px">' + data.name + '</div>' +
                            '<div style="color: #999; font-size: 9px">' + data.email + '</div>' +
                            '</div>' +
                            '</div><div style="clear:both;"></div>'; // make sure we have closed our dom stuff
                    }
                });
                addmember.clear();
                addmember.empty();
                $('#newMessageModal').modal('show');
            });
        });

        $('#newMessagefileIV').on("click",function () {
            $("#newMessageFile").click();
        });

        $('#fileIV').on("click",function () {
            $("#messageFile").click();
        });

        $("#messageFile").change(imageChange);

        $("#newMessageFile").change(imageChangeNewMessage);

        $('#newMessageText_twemoji').on("keyup input",function (e) {
            if (e.which == 13) {
                $('#newSendMessage').trigger('click');
            }
        });

        $('#message_twemoji').on("keyup input",function (e) {

            if (e.which == 13) {
                $('#sendMessage').trigger('click');
            }else{
                onKeyDownNotEnter();
            }
        });

        $('#sendMessage').on("click",function (event) {
            let receiverId=$('#addMember').attr('data-group');
            if(receiverId==null || receiverId==""){
                return;
            }
            $('.close').trigger("click");

            $("#message").find("div:has(br)").each(function(){
                if($(this).html() === '<br>' || $(this).html() === '<br />'){
                    $(this).remove();
                }
            });
            let message=$('#message').text();
            let file=$("#messageFile").val();
            let modmessage=message.replace(/(<([^>]+)>)/ig,"").replace(/&nbsp;/gi," ").replace(/&nbsp;/gi," ").trim();
            if((modmessage == null || modmessage=="") && (file==null || file=="")){
                reset ();
                return;

            }
            if(modmessage != null || modmessage!=""){

                $('#message').val(modmessage);
            }

            let date=moment().format("YYYY-MM-DD");
            let time=moment().format("HH:mm:ss");
            let form=new FormData($('#messageForm')[0]);

            form.append("groupId",receiverId);
            form.append("date",date);
            form.append("time",time);
            reset();
            if(file===null || file===""){

                sendMessage(form,false,false);
            }
            else {
                sendMessage(form,true,false);
            }


        });

        $('#newSendMessage').on("click",function (event) {
            //$('.close').trigger("click");

            let message=$('#newMessageText').text();
            let modmessage=message.replace(/(<([^>]+)>)/ig,"").replace(/&nbsp;/gi," ").replace(/&nbsp;/gi," ").trim();
            let file=$("#newMessageFile").val();
            if((modmessage == null || modmessage=="") && (file==null || file=="")){
                // resetNewMessage();
                return;

            }
            if(modmessage != null || modmessage!=""){

                $('#newMessageText').val(modmessage);
            }


            //let receiverId=$('#addMember').attr('data-group');
            let date=moment().format("YYYY-MM-DD");
            let time=moment().format("HH:mm:ss");
            let userIds=addmember.getValue();
            if(userIds.length==0){
                return;
            }
            let form=new FormData($('#newMessageForm')[0]);
            for(i=0;i<userIds.length;i++){
                form.append("userId[]",userIds[i]);
            }
            form.append("date",date);
            form.append("time",time);

            sendMessage(form,false,true);
            $('#groups').scrollTop(0);
            updateGroupList();

        });

        $('#editGroupName').on("click",function (event) {
            $("#groupName").css("border", "1px solid #ccc");
            $("#changeNameModal").modal("show");

        });

        $("#groupName").focus(function () {
            $(this).css("border", "1px solid #ccc");
        });

        $('#changeNameBtn').on("click",function () {
            let groupId=$('#addMember').attr('data-group');
            let groupName=$("#groupName").val();
            if (groupName==null || groupName==""){
                $('#groupName').css("border","1px solid red");
                toastr.error("Group name is empty");
                return;
            }
            let form=new FormData();
            form.append("groupId",groupId);
            form.append("groupName",groupName);
            let settings = {
                "async": true,
                "crossDomain": true,
                "url": "<?php echo base_url('imApi/changeGroupName') ?>",
                "method": "POST",
                "headers": {
                    "authorization": "Basic YWRtaW46MTIzNA==",
                    "Authorizationkeyfortoken": String(responce),
                    "cache-control": "no-cache",
                    "postman-token": "2a391657-45a9-1a7b-9a67-9b16b0dda13a"
                },
                "processData": false,
                "contentType": false,
                "mimeType": "multipart/form-data",
                "data": form
            };
            $.ajax(settings).done(function (response) {
                toastr.success("Group name update successful");
                $("#changeNameModal").modal("hide");
                //socket.emit('updateData',groupId);
                /*getGroupList(function (data) {
                 if(data){
                 $('#groups li#group_'+groupId).trigger("click",[{update:true}]);
                 }
                 });*/
            })
        });

//------------------  Web sockt section ------------------------------

        socket.on('newMessage',function (res) {

            chatBox.find('#blankImg').hide();
            chatBox.perfectScrollbar();
            let data=res;
            let sender=data.sender;
            let message=data.message;
            let html="";

            if(sender.userId!==parseInt(userId)) {

                html += "<div  class=\"fw-im-message fw-im-isnotme fw-im-othersender\" data-og-container=\"\">";
                if (message.type == "text") {
                    html += "                    <div id='message_" + message.m_id + "' class=\"fw-im-message-text\">" + parseMessage(message.message) + "<\/div>";
                    if(message.linkData!=null){
                        html+=getLinkPreview(JSON.parse(message.linkData),message.link);

                    }
                }
                if (message.type == "image") {
                    html += "<div id='message_" + message.m_id + "' class=\"fw-im-attachments\"><a style=\"width: 200px;height: 200px\" href=\"" + message.message + "\" class=\"ol-hover hover-5 ol-lightbox\"><img height=\"200px\" width=\"200px\" src=\"" + message.message + "\" alt=\"image hover\">";
                    html += "                            <div class=\"ol-overlay ov-light-alpha-80\"><\/div>";
                    html += "                            <div class=\"icons\"><i class=\"fa fa-camera\"><\/i><\/div><\/a>";
                    html += "                            <\/div>";
                }
                if (message.type == "video") {
                    html += "<div id='message_" + message.m_id + "' class=\"fw-im-attachments\" >";
                    html += "                        <video id='video_"+message.m_id+"' class='video-js ' width=\"250px\" height=\"150px\" controls=\"true\" preload='none' name=\"media\"><source src=\"" + message.message + "\" type=\"video\/mp4\"><\/video>";
                    html += "                    <\/div>";
                }
                if(message.type=="audio"){
                    html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\" >";
                    html += "                        <audio id='audio_"+message.m_id+"' class='video-js ' width=\"250px\" height=\"150px\" controls=\"true\" preload='none' name=\"media\"><source src=\""+message.message+"\" type=\"audio\/mp3\"><\/audio>";
                    html += "                    <\/div>";
                }
                html += "                    <div class=\"fw-im-message-author\"  title=\"" + sender.firstName + " " + sender.lastName + "\">";
                html += "                        <img src=\""+sender.profilePictureUrl+"\" >";
                html += "                    <\/div>";
                html += "                    <div class=\"fw-im-message-time\">";
                html += "                        <span style=\"cursor:help\" title=\"" + moment(message.ios_date_time,moment.ISO_8601).format('LLLL') + "\">" + moment(message.ios_date_time,moment.ISO_8601).calendar() + "<\/span>";
                html += "                    <\/div>";
                html += "                <\/div>";
                $.playSound("<?php echo base_url('assets/img/nf')?>");
                toastr.success("New Message from "+ sender.firstName + " " + sender.lastName );
            }else{
                html += "<div  class=\"fw-im-message  fw-im-isme fw-im-othersender\" data-og-container=\"\">";
                if (message.type == "text") {
                    html += "                    <div id='message_"+message.m_id+"' class=\"fw-im-message-text\">" +parseMessage(message.message)  + "<\/div>";
                    if(message.linkData!=null){
                        html+=getLinkPreview(JSON.parse(message.linkData),message.link);

                    }
                }
                if (message.type == "image") {
                    html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\"><a style=\"width: 200px;height: 200px\" href=\"" + message.message + "\" class=\"ol-hover hover-5 ol-lightbox\"><img height=\"200px\" width=\"200px\" src=\"" + message.message + "\" alt=\"image hover\">";
                    html += "                            <div class=\"ol-overlay ov-light-alpha-80\"><\/div>";
                    html += "                            <div class=\"icons\"><i class=\"fa fa-camera\"><\/i><\/div><\/a>";
                    html += "                            <\/div>";
                }
                if (message.type == "video") {
                    html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\" >";
                    html += "                        <video id='video_"+message.m_id+"' class='video-js ' width=\"250px\" height=\"150px\" controls=\"true\" preload='none'  name=\"media\"><source src=\"" + message.message + "\" type=\"video\/mp4\"><\/video>";
                    html += "                    <\/div>";
                }
                if(message.type=="audio"){
                    html += "<div id='message_"+message.m_id+"' class=\"fw-im-attachments\" >";
                    html += "                        <audio id='audio_"+message.m_id+"' class='video-js ' width=\"250px\" height=\"150px\" controls=\"true\" preload='none' name=\"media\"><source src=\""+message.message+"\" type=\"audio\/mp3\"><\/audio>";
                    html += "                    <\/div>";
                }
                html += "                    <div class=\"fw-im-message-author\"  title=\"" + sender.firstName + " " + sender.lastName + "\">";
                html += "                        <img src=\""+sender.profilePictureUrl+"\" >";
                html += "                    <\/div>";
                html += "                    <div class=\"fw-im-message-time\">";
                html += "                        <span style=\"cursor:help\" title=\"" + moment(message.ios_date_time,moment.ISO_8601).format('LLLL') + "\">" + moment(message.ios_date_time,moment.ISO_8601).calendar() + "<\/span>";
                html += "                    <\/div>";
                html += "                <\/div>";

            }

            chatBox.append(html);

            if (message.type == "video") {
                videojs("video_" + message.m_id, {}, function () {
                    // Player (this) is initialized and ready.
                });
            }else if(message.type=="audio"){
                videojs("audio_"+message.m_id, {}, function(){
                    // Player (this) is initialized and ready.
                });
            }
            lightBox.init();
            let groupId=data.to;
            $('#time_'+groupId).html(moment(message.ios_date_time,moment.ISO_8601).fromNow());
            time[groupId]=message.ios_date_time;
            if(message.type=="text"){
                $('#messageType_'+groupId).html(message.message);
            }else{
                $('#messageType_'+groupId).html(message.type);
            }

            let height=chatBox[0].scrollHeight;
            chatBox.scrollTop(height);
            clampData();
        });

        socket.on("userTyping",function (data) {
            var typerGroupId=parseInt(data.groupId);
            var currentGroupId=parseInt($("#addMember").attr("data-group"));


            if(parseInt(data.userId)!==parseInt(userId) && typerGroupId===currentGroupId) {
                let html="";

                html += "<div id='group_"+data.groupId+data.userId+"' class=\"fw-im-message fw-im-isnotme fw-im-othersender\" data-og-container=\"\">";
                html += "                    <div  class=\"fw-im-message-text\" style='background-color: transparent;white-space: unset;'>";

                html += "<div class=\"typing-indicator\">";
                html += "  <span><\/span>";
                html += "  <span><\/span>";
                html += "  <span><\/span>";
                html += "<\/div>";

                html += "<\/div>";
                html += "                    <div class=\"fw-im-message-author\" title=\"" + data.userName + "\">";
                html += "                        <img src=\"" + data.profilePicture + "\" title=\"" + data.userName + "\">";
                html += "                    <\/div>";
                html += "                <\/div>";

                chatBox.append(html);
                let height=chatBox[0].scrollHeight;
                chatBox.scrollTop(height);
                $.playSound("<?php echo base_url('assets/img/typing')?>");
            }

        });

        socket.on("userNotTyping",function (data) {
            var typerGroupId=parseInt(data.groupId);
            var currentGroupId=parseInt($("#addMember").attr("data-group"));
            $("#group_"+data.groupId+data.userId).remove();
            let height=chatBox[0].scrollHeight;
            chatBox.scrollTop(height);

        });

        socket.on("reconnect",function () {
            socket.emit("register",JSON.stringify(tokenData));
            let groupId=$('#addMember').attr("data-group");
            if (groupId!=null || groupId!=""){
                socket.emit("joinRoom",parseInt(groupId));
            }
            $('#connectionErrorModal').modal('hide');
        });

        socket.on("reconnecting",function () {
            $('#connectionErrorModal').modal('show');
        });

        socket.on("updateData",function (res) {
            let currentGroupId = $('#addMember').attr("data-group");
            getGroupList(function (ret) {
                if (ret) {
                    $('.person').removeClass('active');

                    if (document.getElementById('group_' + currentGroupId)) {
                        $('#group_' + currentGroupId).addClass('active');
                        let personName =  $('#group_' + currentGroupId).find('.name').text();
                        getGroupMembers(currentGroupId);
                        printGroupInfo(currentGroupId,groupImages,personName);
                        $('.UserNames').html(personName);

                    } else {
                        $('#groups li').first().trigger("click",[{update:false}]);
                    }

                }else {

                }
            });

        });

        socket.on("pendingMessage",function (res) {

            let currentGroupId=$('#addMember').attr("data-group");
            getGroupList(function (ret) {
                if(ret){

                    let data=JSON.parse(res);
                    let indPending=data.indPending;
                    for(i=0;i<indPending.length;i++){
                        $("#notice_"+indPending[i].groupId).html(indPending[i].pending);
                        $("#notice_"+indPending[i].groupId).removeClass("hidden");
                    }
                    $('.person').removeClass('active');
                    $('#group_'+currentGroupId).addClass('active');
                    if(parseInt(data.totalPending)!=0){

                        $.playSound("<?php echo base_url('assets/img/nf')?>");
                        toastr.success("New message received");
                    }

                }
            });



        });

//------------------ End of web socket section -------------------------

        setInterval(updateTime, 60000);
    });


</script>
</body>
</html>