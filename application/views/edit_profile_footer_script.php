<script type="text/javascript">
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-left",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    $(document).ready(function () {
        $(".jumper").on("click", function( e ) {

            e.preventDefault();

            $("body, html").animate({
                scrollTop: $( $(this).attr('href') ).offset().top
            }, 600);

        });

        var responce=localStorage.getItem("_r");
        var type=jwt_decode(responce).userType;
        if(responce!=null && responce!='' && type==1)
        {
            var id=jwt_decode(responce).userId;
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "<?php echo base_url('user/userProfile/')?>",
                "method": "GET",
                "headers": {
                    "authorization": "Basic YWRtaW46MTIzNA==",
                    "Authorizationkeyfortoken": String(responce),
                    "cache-control": "no-cache",
                    "postman-token": "51901e9b-3613-248b-621e-ffd06d92ded4"
                },
                "processData": false,
                "contentType": false,
                "statusCode": {
                    401: function(error) {
                        location.href="<?php echo base_url('userview/logout') ?>"
                    }
                }
            };

            $.ajax(settings).done(function (response) {

                var data=response;
                $('#firstName').val(data.response.firstName);
                $('#lastName').val(data.response.lastName);
                $('#userEmail').val(data.response.userEmail);
                $("#editProfileImage").fadeIn("fast").attr("src", data.response.profilePictureUrl);
            });
        }
        else {
            location.href="<?php echo base_url("userview/logout")  ?>";
        }
        $("#profileImage").click(function () {

            $("#profileImageFile").click();
        });

        $('#passwordForm').on("submit",function (e) {
            e.preventDefault();
            if(responce!=null || responce!='') {
                var oldPass=$('#userPassword').val();
                var newPass=$('#newPassword').val();
                var rePass=$('#RePassword').val();
                if(oldPass==null || oldPass==""){
                    toastr.error("Old password is empty");
                    return;
                }
                if(newPass==rePass && (oldPass!=null && oldPass!='') && (newPass!=null && newPass!='')){
                    var form = new FormData($(this)[0]);

                    var settings = {
                        "async": true,
                        "crossDomain": true,
                        "url": "<?php echo base_url('/user/changePassword/'); ?>",
                        "method": "POST",
                        "headers": {
                            "authorization": "Basic YWRtaW46MTIzNA==",
                            "Authorizationkeyfortoken": String(responce),
                            "cache-control": "no-cache",
                            "postman-token": "b78ea927-7e7a-4449-3717-a4c2a6f78eca"
                        },
                        "processData": false,
                        "contentType": false,
                        "mimeType": "multipart/form-data",
                        "data": form
                    };

                    $.ajax(settings).done(function (response) {

                        var data=JSON.parse(response);
                        console.log(data);
                        if(data.status.code == 200 && data.status.message=="Success") {

                            toastr.success("Password Update Successful");

                        }
                    }).fail(function (e) {

                        var msg=JSON.parse(e.responseText);

                        if(msg.stauts.code==406) {

                            toastr.error("All Inputs Are Required");

                        }
                        else {

                            toastr.error(msg.stauts.message);

                        }
                    });
                }
                else{

                    toastr.error("New Password And Retype password Doesn't Match");

                }
            }

        });

        $('#profileUpdate').on("submit",function (e) {
            e.preventDefault();
            if(responce!=null || responce!='') {

                var form = new FormData($(this)[0]);
                form.append("userType",1);
                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": "<?php echo base_url('user/edit/'); ?>",
                    "method": "POST",
                    "headers": {
                        "authorization": "Basic YWRtaW46MTIzNA==",
                        "Authorizationkeyfortoken": String(responce),
                        "cache-control": "no-cache",
                        "postman-token": "9c7f3c03-7a40-fd32-88ae-38c983aa53df"
                    },
                    "processData": false,
                    "contentType": false,
                    "mimeType": "multipart/form-data",
                    "data": form,
                    "statusCode": {
                        404: function(error) {
                            var msg=JSON.parse(error.responseText);
                            toastr.error(" "+msg.stauts.message);
                        },
                        401: function(error) {
                            location.href="<?php echo base_url('userview/logout') ?>"
                        }
                    }
                };


                $.ajax(settings).done(function (response) {
                    var data=JSON.parse(response);
                    console.log(data);
                    if(data.status.code == 200 && data.status.message=="Success") {
                        $('#firstName').val(data.response.firstName);
                        $('#lastName').val(data.response.lastName);
                        $('#userEmail').val(data.response.userEmail);

                        toastr.success("Profile Update Successful");

                    }
                    else{

                        toastr.error("Profile Update Unsuccessful");

                    }
                }).fail(function () {

                    toastr.error("Connection !ERROR");

                });
            }
        });

        $("#profileImageFile").change(function (event) {
            var file = this.files[0];
            var imagefile = file.type;
            var match= ["image/jpeg","image/png","image/jpg"];
            if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
            {
                toastr.error("Please Select A Valid Image File");
                return false;
            }
            else
            {
                if(responce!=null || responce!='') {

                    var form = new FormData();
                    form.append("file",file);
                    var settings = {
                        "async": true,
                        "crossDomain": true,
                        "url": "<?php echo base_url('/user/profilePictureUpload/'); ?>",
                        "method": "POST",
                        "headers": {
                            "authorization": "Basic YWRtaW46MTIzNA==",
                            "Authorizationkeyfortoken": String(responce),
                            "cache-control": "no-cache",
                            "postman-token": "b78ea927-7e7a-4449-3717-a4c2a6f78eca"
                        },
                        "processData": false,
                        "contentType": false,
                        "mimeType": "multipart/form-data",
                        "data": form
                    };

                    $.ajax(settings).done(function (response) {
                        var data=JSON.parse(response);

                        $("#editProfileImage").fadeIn("fast").attr("src", data.response);
                    });

                }
            }

        });
    });
</script>