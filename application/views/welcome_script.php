<script type="text/javascript">
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-left",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    $(document).ready(function() {
        localStorage.removeItem("_r");
        $('#loginPassword').keypress(function (e) {
            if (e.which == 13) {
                $("#loginSubmit").trigger('click');
            }
        });
        $("#loginSubmit").on("click",function (e) {
            var form = new FormData();
            form.append("userPassword", $('#loginPassword').val());
            form.append("userEmail", $('#loginEmail').val().trim());
            form.append("<?php echo $this->security->get_csrf_token_name(); ?>","<?php echo $this->security->get_csrf_hash(); ?>");
            if(!loginValid()){
                return;
            }
            $.ajax({
                "async": true,
                "crossDomain": true,
                "url": "<?php echo base_url('registration/login/'); ?>",
                "method": "POST",
                "headers": {
                    "authorization": "Basic YWRtaW46MTIzNA==",
                    "cache-control": "no-cache",
                    "postman-token": "1f556563-4fab-eb2f-d5d1-69ab1613398b"
                },
                "processData": false,
                "contentType": false,
                "mimeType": "multipart/form-data",
                "data": form,
                "success":function (response) {
                    var data=JSON.parse(response);
                    if(data.status.code==200 && data.status.message=="Success")
                    {
                        var	responseToken= data.response;
                        localStorage.setItem("_r",responseToken);
                        location.href="<?php echo base_url('userview/loginSuccess')."?r="; ?>"+responseToken;

                    }

                },
                "statusCode": {
                    404: function(error) {
                        var msg=JSON.parse(error.responseText);
                        toastr.error(msg.status.message);
                    },
                    406: function (error) {
                        var msg=JSON.parse(error.responseText);
                        toastr.error(msg.status.message);
                    }
                }
            });
        });

        $('#regSubmit').on("click",function () {
            var form = new FormData();
            form.append("firstName", $('#regFirstName').val().trim());
            form.append("userPassword", $('#regPassword').val());
            form.append("userEmail", $('#regEmail').val().trim());
            form.append("lastName", $('#regLastName').val().trim());
            form.append("userType", 1);
            form.append("<?php echo $this->security->get_csrf_token_name(); ?>","<?php echo $this->security->get_csrf_hash(); ?>");
            if(!urserRegValid()){
                return;
            }

            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "<?php echo base_url('registration/register/'); ?>",
                "method": "POST",
                "headers": {
                    "authorization": "Basic YWRtaW46MTIzNA==",
                    "cache-control": "no-cache",
                    "postman-token": "4872bfd4-6509-785c-630b-6677aea9511b"
                },
                "statusCode": {
                    406: function(error) {

                        toastr.error(" One or more Input Fields Are Empty")
                    },
                    409:function(error) {

                        toastr.error(" "+" Email already exist!");
                    }
                },
                "processData": false,
                "contentType": false,
                "mimeType": "multipart/form-data",
                "data": form
            };

            $.ajax(settings).done(function (response) {
                var data=JSON.parse(response);
                if(data.status.code==200 )
                {
                    toastr.success(" "+" Registration Successful. You can login now");
                }

            });
        });

        <?php
        if($token!=null && $token!=''){
        ?>

        var verifyToken="<?php echo $token ?>";
        if(verifyToken!=null && verifyToken!=''){
            var userEmail=jwt_decode(verifyToken).userEmail;
            var form = new FormData();
            form.append("userToken", verifyToken);
            form.append("userEmail", userEmail);
            form.append("<?php echo $this->security->get_csrf_token_name(); ?>","<?php echo $this->security->get_csrf_hash(); ?>");
            $.ajax({
                "async": true,
                "crossDomain": true,
                "url": "<?php echo base_url('registration/emailVerification/'); ?>",
                "method": "POST",
                "headers": {
                    "authorization": "Basic YWRtaW46MTIzNA==",
                    "cache-control": "no-cache",
                    "postman-token": "1f556563-4fab-eb2f-d5d1-69ab1613398b"
                },
                "processData": false,
                "contentType": false,
                "mimeType": "multipart/form-data",
                "data": form,
                "success":function (response) {
                    var data=JSON.parse(response);
                    if(data.status.code==200)
                    {
                        toastr.success(" "+data.status.message);

                    }

                },
                "statusCode": {
                    404: function(error) {
                        var msg=JSON.parse(error.responseText);

                        toastr.error(" "+msg.status.message);

                    },
                    406: function (error) {
                        var msg=JSON.parse(error.responseText);
                        toastr.error(" "+msg.status.message);
                    }
                }
            });
        }


        <?php
        }
        ?>
        function loginValid(){
            var userPassword= $('#loginPassword').val();
            var userEmail=$('#loginEmail').val().trim();

            var ret = true;
            if (userEmail == null || userEmail == "" ) {
                ret = false;
                $('#loginEmail').css("border", "2px solid red");
            }
            if (userPassword == null || userPassword == "") {
                ret = false;
                $('#loginPassword').css("border", "2px solid red");
            }
            return ret;
        }
        function urserRegValid() {
            var firstName = $('#regFirstName').val().trim();
            var userPassword = $('#regPassword').val();
            var userEmail = $('#regEmail').val().trim();

            var ret = true;
            if (firstName == null || firstName == "") {
                ret = false;
                $('#regFirstName').css("border", "2px solid red");
            }
            if (userEmail == null || userEmail == "" ) {
                ret = false;
                $('#regEmail').css("border", "2px solid red");
            }
            if (userPassword == null || userPassword == "") {
                ret = false;
                $('#regPassword').css("border", "2px solid red");
            }
            if(!isEmail(userEmail)  && ret!=false){
                ret=false;
                $('#ErrorBlock5').addClass("hidden");
                $('#ErrorBlock2').removeClass("hidden");
                $('.error-message2').html(" "+" Email is not valid");
                $('#regEmail').css("border", "2px solid red");
            }
            return ret;
        }

        function  isPhone(number) {
            var regex = /^[0-9-+]+$/;
            return regex.test(number);
        }
        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }
    });

</script>

</body>

</html>