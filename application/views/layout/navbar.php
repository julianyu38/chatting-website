
<body class="fullwidth sticky-header">
<div id="wrapper" class="regular-layout">
    <!-- #####Begin Header-->
    <header id="header" class="header-thin" style="background-color: transparent">
        <!-- #####Begin header main area-->
        <div class="head-main">
            <div class="ol-particles h-100" id="ol-particles-yjufr"><canvas class="particles-js-canvas-el" width="609" height="200" style="width: 100%; height: 100%;"></canvas>
            <div class="container">

                <!-- #####Begin logo wrapper-->
                <div class="logo-wrapper">
                    <!-- standard Logo--><a href="<?=base_url('userview/im')?>"><img src="<?php echo base_url('assets/img/imt.png');?>" class="logo-light ol-retina" style="height: 30px;"></a>
                </div>
                <!-- #####End logo wrapper-->
                <!-- #####Begin primary menu-->
                <ul id="primary-menu">
                    <!-- #####Begin menu item-->
                    <!-- #####Begin menu item-->
                    <li class="mega-menu menu-item-has-children"><a title="Settings"><span>Settings</span></a>
                        <!-- #####Begin megamenu-->
                        <ul class="sub-menu">
                            <!-- #####Begin menu item-->

                            <!-- #####End menu item-->
                            <!-- #####Begin menu item-->
                            <li ><a href="<?=base_url('userview/profile')?>" title="Log Out"><span>Profile</span></a>
                            <li ><a href="<?=base_url('userview/logout')?>" title="Log Out"><span>Log Out</span></a>
                                <!-- #####Begin submenu-->

                                <!-- #####End menu item-->
                            </li>
                            <!-- #####End menu item-->
                            <!-- #####Begin menu item-->
                        </ul>
                        <!-- #####End megamenu-->
                    </li>

                    <!-- #####End menu item-->
                </ul>
                <!-- #####End primary menu-->
                <div class="header-icons">
                    <!-- #####Begin cart-->

                    <!-- #####End cart-->
                    <!-- #####Begin trigger for search-->

                    <!-- #####End trigger for search-->
                    <!-- #####Begin mobile menu trigger-->
                    <div class="ol-mobile-trigger hamburger hamburger--elastic">
                        <div class="hamburger-box">
                            <div class="hamburger-inner"></div>
                        </div>
                    </div>
                    <!-- #####End mobile menu trigger-->

                    <!-- #####Begin icons beside menu-->

                    <!-- #####End icons beside menu-->
                </div>
            </div>
            <!-- #####End header main area-->
    </header>
</div>
    <!-- #####End Header-->
    <!-- #####Begin contents-->


