/**
 * Created by Farhad Zaman on 3/11/2017.
 */
module.exports={
    dbSettings:function () {
        return {
            connectionLimit : 100,
            host : '127.0.0.1',
            user : 'root',
            password : 'root',
            database : 'im_messenger'
        };
    }
};


