/**
 * Created by Farhad Zaman on 2/13/2017.
 */
"use strict";
const express = require("express");
const app = express();
const server = require("http").createServer(app);
const io = require("socket.io").listen(server);
const jwt_decode = require("jwt-decode");
const jwt = require('jwt-simple');
const mysql = require("mysql");
const moment = require('moment');
const database = require('./databaseConfig');
const extract = require('meta-extractor');
const getUrl=require('get-urls');
const { URL } = require('url');
const url2 = require('url');

let CONSUMER_SECRET="yYNIn86DMxSiGSarZehUZ"; //need to verify jwt tokens;
let users={};
let tokens={};
let connections=[];

server.listen(process.env.PORT || 8080,function () {
    let host = server.address().address;
    let port = server.address().port;
    console.log("\n\n---------------------------- "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ----------------------------\n");
    console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"Im server running at http://"+host+':'+port);
});


const mysqlCon = mysql.createPool(database.dbSettings());


let db = function (conn, query, calback) {
    conn.getConnection(function (err, connection) {
        if (err) {
            console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "error connecting : " + err.stack);
            return;
        }
        console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + "DB connected as id :" + connection.threadId);


        connection.query(query, function (error, results, fields) {
            connection.release();
            if (error) {
                console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + error.code); // 'ECONNREFUSED'
                console.log("[ " + moment().format('MMMM Do YYYY, hh:mm:ss') + " ] " + error.fatal); // true
            }
            // connected!

            calback(results, error);

        });
    });
};




app.use(express.static(__dirname+'/public'));



app.get("/",function (req,res) {
   res.sendFile(__dirname+"/index.html");

});




    io.on("connection", function (socket) {

        connections.push(socket);
        users[socket.id] = socket;
        let roomId = null;
        console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"connected %s", connections.length);

        function DeleteSocket(socketId) {
            let deleteSocketIdQ = "DELETE FROM `im_usersocket` WHERE  socketId='" + socketId + "'";
            db(mysqlCon, deleteSocketIdQ, function (res, err) {
                if (err) {
                    console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"socket id delete failed");
                }
                else {
                    console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"socket id delete success");
                }
            });
        }

        socket.on("disconnect", function () {

            let connectionIndex = connections.indexOf(socket);
            DeleteSocket(socket.id);
            if (roomId !== null) {
                socket.leave(roomId);
                console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"leaving room '%s' on disconnect", roomId);
            }

            connections.splice(connectionIndex, 1);
            if(socket.id in users){
                delete users[socket.id];
            }

            console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"disconnected %s", connections.length);

        });

        // groupId (int)
        socket.on("joinRoom", function (groupId) {
            roomId = "room-" + groupId;
            socket.join(roomId);
           // console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"joinRoom:" + groupId);
        });

        socket.on("leaveRoom", function (groupId) {
            socket.leave("room-" + groupId);
           // console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"leaveRoom:" + groupId);
        });

        socket.on("notTyping",function (responce) {
            let data = null;
            if(typeof responce === "object"){
                data = responce;
            }else{
                data = JSON.parse(responce);
            }
            let tokenData=jwt_decode(data._r);
            let senderData={
                userName:tokenData.userName,
                profilePicture:tokenData.profilePicture,
                userId:tokenData.userId,
                groupId:data.groupId
            };

            io.sockets.in("room-" + data.groupId).emit("userNotTyping", senderData);
            //console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"notTyping:" + tokenData.userId);
        });

        socket.on("typing",function (responce) {
            let data = null;
            if(typeof responce === "object"){
                data = responce;
            }else{
                data = JSON.parse(responce);
            }
            let tokenData=jwt_decode(data._r);
            let senderData={
                userName:tokenData.userName,
                profilePicture:tokenData.profilePicture,
                userId:tokenData.userId,
                groupId:data.groupId
            };
            io.sockets.in("room-" + data.groupId).emit("userTyping", senderData);
            //console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"typing:" + tokenData.userId);
        });

        socket.on("register", function (responce) {

            try {

                let data = null;
                if(typeof responce === 'object'){
                    data = responce;
                }else{
                    data = JSON.parse(responce);
                }
                isValidToken(data._r, function (res) {
                    if (res) {

                        tokens[socket.id] = data._r;
                        try {
                            let user = jwt_decode(data._r);
                            let insertSocketQ = "INSERT INTO `im_usersocket` (`serial`, `userId`, `socketId`) VALUES (NULL, '" + user.userId + "', '" + socket.id + "')";
                            db(mysqlCon, insertSocketQ, function (res, err) {
                                if (err) {
                                    console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"socket id insert failed");

                                }
                                else {
                                    console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"socket id insert success");
                                }
                            });
                        } catch (err) {
                            console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+err);
                        }

                    } else {

                        socket.disconnect();
                        console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"invalid user");
                    }
                });
            }catch (err){
                console.log(err);
            }

        });

        function pendingMessage(userId) {
            let query = "SELECT COUNT(m_id) as pending, g_id as groupId FROM `im_receiver` WHERE r_id=" + userId + " and received=0 GROUP BY g_id";
            db(mysqlCon, query, function (res, err) {
                if (err) {
                    console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"error in: messageNotification");
                } else {
                    let sendData = {"totalPending": res.length, "indPending": res};
                    let findSocketIdQ = "select socketId from im_usersocket where userId=" + userId;
                    db(mysqlCon, findSocketIdQ, function (result, error) {
                        if (error) {
                            console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"get socketId failed");
                        } else {
                            for (let i = 0; i < result.length; i++) {
                                try{
                                    users[result[i].socketId].emit("pendingMessage", JSON.stringify(sendData));
                                }
                                catch (err){
                                    DeleteSocket(result[i].socketId);
                                    console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+err);
                                }
                            }
                        }
                    });
                }
            });
        }

        
        socket.on("updateMember",function (res) {
            let data = null;

            if(typeof res === 'object'){
                data=res;
            }else {
                data = JSON.parse(res);
            }
            isValidToken(data._r, function (result) {
                if (result) {
                    try{
                        let members = data.memberIds;
                        if (members !== null) {
                            for (let i = 0; i < members.length; i++) {
                                let findSocketIdQ = "select socketId from im_usersocket where userId=" + members[i].u_id;
                                db(mysqlCon, findSocketIdQ, function (result, error) {
                                    if (error) {
                                        console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"get socketId failed");
                                    } else {
                                        for (let i = 0; i < result.length; i++) {
                                            try {
                                                users[result[i].socketId].emit("updateData", data.groupId);
                                            }
                                            catch (err) {
                                                DeleteSocket(result[i].socketId);
                                                console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+err);
                                            }
                                        }
                                    }
                                });
                            }
                        }

                    }catch (err){
                        console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+err);
                    }
                } else {
                    socket.disconnect();
                    console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"invalid user");
                }


            });

        });

        socket.on("messageNotification", function (response) {

            let data = null;

            if(typeof response === 'object'){
                data=response;
            }else {
                data = JSON.parse(response);
            }
            try{
                let userId = jwt_decode(data._r).userId;

                isValidToken(data._r, function (res) {
                if (res) {
                    pendingMessage(userId);
                } else {
                    socket.disconnect();
                    console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"invalid user");
                }
            });
            }catch (err){
                console.log(err);
            }
        });

        socket.on("sendMessage", function (response) {
            let data = null;
            let message=null;
            if(typeof response === 'object'){
                data=response;
            }else {
                data = JSON.parse(response);
            }


            isValidToken(data._r, function (ret) {
                if (ret) {
                    let res = data.receiversId;
                    let receiversRoomId = data.to;

                    for (let i = 0; i < res.length; i++) {
                        (function () {
                            let uid = res[i].u_id;
                            let findSocketIdQ = "select socketId from im_usersocket where userId=" + uid;
                            db(mysqlCon, findSocketIdQ, function (result, err) {
                                let receiverQuery;
                                if (err) {
                                    console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"get socketId failed");
                                } else {
                                    if (result.length > 0) {
                                        for (let i = 0; i < result.length; i++) {
                                            try {
                                                if (!io.sockets.adapter.sids[result[i].socketId]["room-" + receiversRoomId]) {
                                                    receiverQuery = "INSERT INTO `im_receiver` (`serial`,`g_id`, `m_id`, `r_id`, `received`, `time`) VALUES (NULL,'" + data.to + "', '" + data.message.m_id + "', '" + uid + "', '0', '" + data.message.date_time + "');";
                                                    db(mysqlCon, receiverQuery, function (res, err) {
                                                        if(err){
                                                            console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"Message saving failed");
                                                        }else {
                                                            console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"message saved to receiver DB");
                                                            pendingMessage(uid);
                                                        }

                                                    });
                                                }
                                            } catch (app) {
                                                console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+err);
                                            }
                                        }
                                    } else {         //user has no active session
                                        receiverQuery = "INSERT INTO `im_receiver` (`serial`,`g_id`, `m_id`, `r_id`, `received`, `time`) VALUES (NULL,'" + data.to + "', '" + data.message.m_id + "', '" + uid + "', '0', '" + data.message.date_time + "');";
                                        db(mysqlCon, receiverQuery, function (res, err) {
                                            console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"message saved to receiver DB");
                                        });
                                    }
                                }
                            });

                        })();
                    }

                    messageConstructLinkConvert(data,function (data) {
                        message=data;
                        io.sockets.in("room-" + receiversRoomId).emit('newMessage', message);
                    });

                } else {
                    socket.disconnect();
                    console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"invalid user");
                }
            });
        });

    });

    function messageConstructLinkConvert(data,callback){
        let message=null;
        let mainMessage=data.message.message;
        let mainUrl=null;
        let host=null;
        let title=null;
        let description=null;
        let playerOrImageUrl=null;

        if(hasUrl(mainMessage)){
            let url=getFirstUrl(mainMessage);
            let responded=false;
            extract({
                    uri: url,
                    timeout:3000,
                    headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.98 Mobile Safari/537.36' }
            },
                function (err, res){
                    if(!err ){
                        responded=true;
                        res.url=url;
                        mainUrl=res.url;
                        host=res.host;
                        title=getTitle(res);
                        description=getDescription(res);
                        playerOrImageUrl=getPlayerOrImageUrl(res);
                        let linkData={mainUrl:mainUrl,host:host,title:title,description:description,playerOrImageUrl:playerOrImageUrl};
                        let linkDataQ="update `im_message` set `link`="+mysqlCon.escape(mainUrl)+",`linkData`="+mysqlCon.escape(JSON.stringify(linkData))+" where m_id="+data.message.m_id;
                        db(mysqlCon,linkDataQ,function (res, err) {
                            if(err){
                                console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"Message link data update failed");
                                callback(messageWithOutLink(data));
                            }
                            else {
                                console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"Message link data update Success");

                                callback(messageWithLink(data,mainUrl,linkData));
                            }
                        });
                    }else if(!responded){
                        responded=true;
                        console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+"Message link fetch failed");
                        callback(messageWithOutLink(data));
                    }

                });
        }else {

            callback(messageWithOutLink(data));
        }

    }

     function hasUrl(message){
         let surl=getUrl(message);
         let urlArray = Array.from(surl);
         return urlArray.length > 0;
    }

    function getFirstUrl(message){
        let surl=getUrl(message,{stripWWW:false});
        let urlArray = Array.from(surl);
        return urlArray[0];

    }

    function getTitle(res) {
        let mainTitle=null;
        if(res.ogTitle!==undefined){
            mainTitle=res.ogTitle;
        }else if(res.twitterTitle!==undefined){
            mainTitle=res.twitterTitle;
        }else if(res.title!==undefined){
            mainTitle=res.title;
        }else{
            mainTitle=res.host;
        }
       // return mainTitle;
        return mainTitle.replace(/[\n\r]+/g, '').replace(/\s{2,10}/g, ' ').trim();
    }

    function getDescription(res) {
        let description=null;
        if(res.ogDescription!==undefined){
            description=res.ogDescription;
        }else if(res.twitterDescription!==undefined){
            description=res.twitterDescription;
        }else if(res.description!==undefined){
            description=res.description;
        }else{
            description=''
        }

        return description.replace(/[\n\r]+/g, '').replace(/\s{2,10}/g, ' ').trim();
        //return description
    }

    function getPlayerOrImageUrl(res) {
        let url={url:null,type:null};
        if(res.twitterPlayer!==undefined){
            url.url=res.twitterPlayer;
            url.type='player';
        }else if(res.ogVideoUrl!==undefined){
            url.url=res.ogVideoUrl;
            url.type='player';
        }else if(res.twitterImage!==undefined){
            url.url=imageUrlFormat(res,res.twitterImage);
            url.type='image';
        }else if(res.ogImage!==undefined){
            url.url=imageUrlFormat(res,res.ogImage);
            url.type='image';
        }
        return url;
    }

    function imageUrlFormat(res,imageUrl){
        let formattedUrl=imageUrl;
        let url=new URL(res.url);
        const urlParts = url2.parse(formattedUrl);
        if(urlParts.host===null){
            formattedUrl=url.origin+formattedUrl;
        }
        return formattedUrl;
    }
    function messageWithLink(data,mainUrl,linkData) {
        return{
            "to": data.to,
            "message": {
                "m_id": data.message.m_id,
                "message": data.message.message,
                "type": data.message.type,
                "receiver_type": data.message.receiver_type,
                "date": data.message.date,
                "time": data.message.time,
                // "date_time": data.message.date_time,\
                "link": mainUrl,
                "linkData": JSON.stringify(linkData),
                "ios_date_time": data.message.ios_date_time

            },
            "sender": {
                "userId": data.sender.userId,
                "firstName": data.sender.firstName,
                "lastName": data.sender.lastName,
                "userEmail": data.sender.userEmail,
                "userStatus": data.sender.userStatus,
                "profilePictureUrl": data.sender.profilePictureUrl
            }

        };
    }

    function messageWithOutLink(data) {
        return{
            "to": data.to,
            "message": {
                "m_id": data.message.m_id,
                "message": data.message.message,
                "type": data.message.type,
                "receiver_type": data.message.receiver_type,
                "date": data.message.date,
                "time": data.message.time,
                // "date_time": data.message.date_time,\
                "link": null,
                "linkData": null,
                "ios_date_time": data.message.ios_date_time

            },
            "sender": {
                "userId": data.sender.userId,
                "firstName": data.sender.firstName,
                "lastName": data.sender.lastName,
                "userEmail": data.sender.userEmail,
                "userStatus": data.sender.userStatus,
                "profilePictureUrl": data.sender.profilePictureUrl
            }

        };
    }
    function isValidToken(token, callback) {
        try{
            let userSecret = jwt.decode(token,CONSUMER_SECRET).consumerKey;
            let query = "select `userId` from `users` where `userSecret`='" + userSecret + "'";
            db(mysqlCon, query, function (res, err) {
                if (err) {
                    console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+err);
                    callback(false);
                }else{
                    if (res.length === 0) {
                        callback(false);
                    }
                    else {
                        callback(true);
                    }
                }

            });
        }catch (err){
            console.log("[ "+moment().format('MMMM Do YYYY, hh:mm:ss')+" ] "+err);
            callback(false);
        }

    }

